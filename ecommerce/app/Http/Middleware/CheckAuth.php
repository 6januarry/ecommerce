<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class CheckAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check())
            {
                return redirect()->route('getAdminLogin');
            }
        if(Auth::check()&&Auth::user()->level!=0)
            {
                Auth::logout();
                return redirect()->route('getAdminLogin');
            }
        return $next($request);
    }
}
