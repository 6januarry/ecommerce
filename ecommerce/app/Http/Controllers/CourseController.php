<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests;
use App\products;
use App\picture;
use App\slide;
use App\category;
use App\category_details;
use DateTime;

class CourseController extends Controller
{
     

    public function getBestSellers()
    {
        $products= DB::table('products')->where('products.ranking','>','5')->paginate(5);
        // $products =  products::join('picture','products.id','=','picture.id_products')->select('products.*','picture.*')->where('products.ranking','>','5')->paginate(5);
        return $products;
    }
     public function getProducts()
    {
        $products =   DB::table('products')->paginate(5);
        return $products;
    }
    public function getProductDetails($id)
    {
        // $products = products::select('products.*')->where('id','=',$id)->get();
        $product_details = DB::table('products')->join('product_details','products.id','=','product_details.id_products')->select('products.*','product_details.*')->where('products.id','=',$id)->get();

        return view('page.content.product_detail',['product_details' => $product_details[0]]);
        // return dd($product_details);



    }
    public function getSlide()
    {
    	$slides = slide::get();
    	return $slides;
    }
    public function getCategory()
    {
        $category = category::get();
        return $category;
    }
    public function postMessage(Request $request)
    {
        $a=DB::table('customer_care')->insertGetId(['name' => $request->name, 'email' => $request->email,'message'=>$request->comment,'created_at'=>new DateTime()]);
        return redirect()->back()->with('alert', 'Your message has been sent!');
    }
}
