<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Orders;
use Illuminate\Support\Facades\DB;
use Charts;
use Illuminate\Support\Facades\App;
class Deliver extends Controller
{
    public function getLogin()
    {
    	if(!Auth::check())
    		return view('admindeliver.signin')->with('a','1');
    	else
        {

    		return redirect('deliver_admin');
        }
    }
    public function postLogin(Request $request)
    {
    	$login=[
    		'email' => $request->txtemail, 
    		'password' => $request->txtpassword,
    		'level' => 1
    	];
    	if (Auth::attempt($login)) {
            // Authentication passed...
            return redirect('deliver_admin');
        }
        else
        	return redirect()->back()->with('error_login','Login unsuccessful');
    }
    public function getLogOut()
    {
    	auth::logout();
    	return redirect()->route('getLogin');
    }
    public function getList()
    {
        $orders = DB::table('orders')
            ->join('ec_users', 'orders.id_customer', '=', 'ec_users.id')
            ->select('orders.*', 'ec_users.first_name', 'ec_users.last_name')
            ->where('orders.status','=','0')
            ->orderBy('id')
            ->paginate(5);
            $pusher = App::make('pusher');
            $pusher->trigger( 'deliver','neworders',$orders);
        //return redirect()->route('index');
            return $orders;
    }
    public function getChart()
    {
        $a= DB::table('orders')
            ->select('id','order_date')
            ->get();
        $chart = Charts::database($a, 'bar', 'highcharts')
                    ->title('Number Of Orders')
                    ->elementLabel('Order')
                    ->dateColumn('order_date')
                    ->groupByMonth();
        return view('admindeliver.statistic',['chart' => $chart]);
    }
    public function getCount()
    {
        $b=DB::table('orders')->where('status','=','0')->count();
        $pusher = App::make('pusher');
        $pusher->trigger( 'deliver','count',$b);
        return redirect()->route('index');
    }
    public function getCancel()
    {
        $c=DB::table('orders')->where('status','=','2')->count();
        $pusher = App::make('pusher');
        $pusher->trigger( 'deliver','cancel',$c);
        return redirect()->route('index');
    }
    public function getCountProducts()
    {
        $d=DB::table('products')->sum('quantity');
        $pusher = App::make('pusher');
        $pusher->trigger( 'deliver','products',$d);
        return redirect()->route('index');
    }
    // public function getCompany($id)
    // {
    //     $e=DB::table('shipper')->where('id_user','=',$id)->get();
    //     return $e;
    // }
    public function changePass(Request $request)
    {
                $request->user()->fill([
                'password' => Hash::make($request->new)
            ])->save();
                Auth::logout();
        
    }
    public function postStatus(Request $request, $id)
    {
        $a=DB::table('orders')->where('id','=',$id)->update(['status'=>$request->toArray()[0]]);
        //dd($request->toArray());
    }
}
