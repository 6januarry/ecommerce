<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Charts;
use DateTime;
use App\User;
use Illuminate\Support\Facades\Hash;
class Admin extends Controller
{
    //
    public function getLogin()
    {
    	if(!Auth::check())
    		return view('admin.login');
    	else
        {

    		return redirect('admin_panel');
        }
    }
    public function postLogin(Request $request)
    {
    	$login=[
    		'email' => $request->email, 
    		'password' => $request->password,
    		'level' => 0
    	];
    	if (Auth::attempt($login)) {
            // Authentication passed...
            return redirect()->route('index_admin');
        }
        else
        	return redirect()->back();
    }
    public function getLogOut()
    {
    	auth::logout();
    	return redirect()->route('getAdminLogin');
    }
    public function addImage(Request $request)
    {
        // while($request->hasFile('file'))
        //     {
        //         //$file = $request->file('file');
        //         $request->file('file')->move('uploads', $request->file('file')->getClientOriginalName());
        //         //$file->move('uploads',$file->getClientOriginalName());
        //     }
        if ($request->hasFile('file'))
        {
            $imageName = time().'.'.$request->file('file')->getClientOriginalExtension();
            $a=$request->file('file')->getClientOriginalName();
            $request->file('file')->move(public_path() . '/uploads/', $a);
            //return response()->json(['success'=>$imageName]);
        }
    }
    public function getOrderToday()
    {
        
        $orders = DB::table('orders')
            ->whereDate('order_date','=',Carbon::today())
            ->count();
            $pusher = App::make('pusher');
            $pusher->trigger( 'admin','orderstoday',$orders);
        return redirect()->route('index_admin');
            //return $orders;
    }
    public function getOrderMonthly()
    {
        
        $orders = DB::table('orders')
            ->whereMonth('order_date','=',Carbon::today()->month)
            ->count();
            $pusher = App::make('pusher');
            $pusher->trigger( 'admin','ordersmonthly',$orders);
        return redirect()->route('index_admin');
            //return $orders;
    }
    public function revenue()
    {
        $d=DB::table('orders')
            ->whereMonth('order_date','=',Carbon::today()->month)
            ->sum('paid');
        $pusher = App::make('pusher');
        $pusher->trigger( 'admin','revenue',$d);
        return redirect()->route('index_admin');
    }
    public function getChart()
    {
        $a= DB::table('ec_users')
            ->select('id','created_at')
            ->get();
        $chart = Charts::database($a, 'line', 'highcharts')
                    ->title('Numbers Of User')
                    ->elementLabel('User')
                    ->dateColumn('created_at')
                    ->groupByMonth();
        return view('admin.layout.index',['chart' => $chart]);
    }
    public function getCategory()
    {
        $category = DB::table('category')
            ->join('category_details', 'category.id', '=', 'category_details.id_category')
            ->select('category_details.id','category_details.name', 'category.category_name','category_details.active')
            ->get();
        return $category;
    }
    public function cateEdit($id)
    {
        $category = DB::table('category')
            ->join('category_details', 'category.id', '=', 'category_details.id_category')
            ->select('category_details.id','category_details.name', 'category.category_name','category_details.active')
            ->where('category_details.id','=',$id)
            ->get();
        //$a=$category->toArray()[0];
        //dd($a[0]->id);
        return $category;
    }
    public function updateEdit(Request $request,$id)
    {
        $temp;
        if($request->category_name=='Clothing')
            $temp=1;
        else if($request->category_name=='Shoes')
            $temp=2;
        else $temp=3;
        $a=DB::table('category_details')
            ->where('id','=',$id)
            ->update(['id_category'=>$temp,'name'=>$request->name,'active'=>$request->active,'updated_at'=>new DateTime()]);
    }
    public function addCate(Request $request)
    {
        $id=DB::table('category_details')
            ->insertGetId(['name' => $request->name, 'id_category' => $request->id_category,'active'=>$request->active,'created_at'=>new DateTime()]);
            dd(new DateTime());
    }
    public function delCate($id)
    {
        $a=DB::table('category_details')->where('id','=',$id)->delete();
    }
    public function getCateDetails($id)
    {
        $a=DB::table('category_details')->where('id_category','=',$id)->get();
        return $a;
    }
    public function getProds($id)
    {
        $a=DB::table('products')
            ->join('product_details','products.id','=','product_details.id_products')
            ->select('products.*','product_details.*')
            ->where('id_category','=',$id)->paginate(5);
        return $a;
    }
    public function getProduct($id)
    {
        $a=DB::table('products')
            ->join('product_details','products.id','=','product_details.id_products')
            ->select('products.*','product_details.*')
            ->where('products.id','=',$id)->get();
        return $a;
    }
    public function postProduct(Request $request,$id)
    {
        // if ($request->hasFile('images'))
        // {
        //     $imageName = time().'.'.$request->file('images')->getClientOriginalExtension();
        //     $request->file('images')->move(public_path() . '/uploads/', $imageName);
        //     //return response()->json(['success'=>$imageName]);
        //     $a=DB::table('products')
        //     ->where('id','=',$id)
        //     ->update(['product_name'=>$request->product_name,'description'=>$request->description,'quantity'=>$request->quantity,'price_unit'=>$request->price_unit,'discount'=>$request->discount,'ranking'=>$request->ranking,'picture'=>'uploads/'+$imageName]);
        // }
        // else
        //{
            $a=DB::table('products')
            ->join('product_details','products.id','=','product_details.id_products')
            ->where('product_details.id','=',$id)
            ->update(['product_name'=>$request->product_name,'description'=>$request->description,'quantity'=>$request->quantity,'price_unit'=>$request->price_unit,'discount'=>$request->discount,'ranking'=>$request->ranking,'picture'=>$request->picture]);
        //}   
    }
    public function delProds($id)
    {
        $a=DB::table('product_details')->where('id','=',$id)->delete();
    }
    public function findProdsOrders($id)
    {
        $a=DB::table('order_details')->where('id_product_details','=',$id)->count();
        return $a;
    }
    public function getUsers()
    {
        $a=User::paginate(5);
        return $a;
    }
    public function delUser($id)
    {
        $b=DB::table('ec_users')
            ->join('orders','ec_users.id','=','orders.id_customer')
            ->where('ec_users.id','=',$id)
            ->get();
        if($b->toArray()==null)
        {
            $c=DB::table('shipper')
                ->where('id_user','=',$id)
                ->get();
            $d=DB::table('orders')
                ->where('id_shipper','=',$c->toArray()[0]->id)
                ->get();
            if($d->toArray()==null && $c->toArray()==null)
            {
                $a=User::find($id);
                $a->delete();
                return 'true';
            }
        }
        return 'false';
    }
    public function getUserDetails($id)
    {
        $a=User::find($id);
        $a=$a->toArray();
        return $a;
    }
    public function postUserDetails(Request $request,$id)
    {
        // $temp;
        // if($request->level=='Deliver')
        //     $temp=1;
        // else if($request->level=='Member')
        //     $temp=2;
        User::where('id',$id)
            ->update(['first_name'=>$request->first_name,'last_name'=>$request->last_name,'level'=>$request->level,'active'=>$request->active,'updated_at'=>new DateTime()]);
    }
    public function postUserAdd(Request $request)
    {
        $user=new User;
        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->email=$request->email;
        $user->password=Hash::make($request->password);
        $user->level=$request->level;
        $user->save();
    }
    public function getMail($mail)
    {
        $a=User::where('email',$mail)->get();
        if($a->toArray()==null)
            return 'true';
        return 'false';
    }
}
