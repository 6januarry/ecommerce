@extends('admindeliver.master')
@section('content')
<div class="contaier-fluid mt-5">
	<div class="row">
		<div class="col-md-10 push-1">
			<div class="container mt-5 mb-3">
				<div class="app">
		            <center>
		                {!! $chart->html() !!}
		            </center>
        		</div>
        		{!! Charts::scripts() !!}
        		{!! $chart->script() !!}
			</div>
		</div>
	</div>
</div>  
@endsection