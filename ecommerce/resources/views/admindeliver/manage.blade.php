@extends('admindeliver.master')
@section('content')
<div class="contaier-fluid mt-5" ng-controller="controller">
	<div class="row">
		<div class="col-md-10 push-1">
			<div class="container mt-5 mb-3">
				<table class="table table-hover">
				  	<thead>
				    	<tr>
						    <th class="text-center">ID</th>
						    <th class="text-center">Customer</th>
						    <th class="text-center">Address</th>
						    <th class="text-center">Order Date</th>
						    <th class="text-center">Status</th>
				    	</tr>
				  	</thead>
				  	<tbody>
					    <tr ng-repeat="s in orders">
						    <td class='text-center'><%s.id%></td>
						    <td class='text-center'><%s.first_name%> <%s.last_name%></td>
						    <td class='text-center'><%s.address%></td>
						    <td class='text-center'><%s.order_date%></td>
						    <td class='text-center'>
						      	<button type="button" class="btn btn-sm btn-outline-info" ng-click="state('receive',s.id)">Received</button>
						      	<button type="button" class="btn btn-sm btn-outline-danger" ng-click="state('cancel',s.id)">Canceled</button>
						     </td>
					    </tr>
				  	</tbody>
				</table>
				<posts-pagination class="text-center"></posts-pagination>
			</div>
		</div>
	</div>
</div>
@endsection