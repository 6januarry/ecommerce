@extends('admindeliver.master')
@section('content')
<div class="container-fluid mt-5" ng-controller="controller2">
	<div class="row">
		<div class="col-md-6 push-3 mt-5">
			<div class="container mt-4">
				<h3 class="m-0 text-center">Personal Information</h3><hr>
				<div id="personal" class="container push-4 ml-1">
					<div>
						<img src="{!!asset('ec/template/images/icon/png/user.png')!!}" alt="">
						<p class="d-inline-block">{!! Auth::user()->first_name !!} {!! Auth::user()->last_name !!}</p>
					</div>
					<div>
						<img src="{!!asset('ec/template/images/icon/png/age.png')!!}" alt="">
						<p class="d-inline-block">{!! Auth::user()->age !!}</p>
					</div>
					<div>
						<img src="{!!asset('ec/template/images/icon/png/city.png')!!}" alt="">
						<p class="d-inline-block">{!! Auth::user()->city !!}</p>
					</div>
					<!-- <div>
						<img src="{!!asset('ec/template/images/icon/png/company.png')!!}" alt="">
						<p class="d-inline-block" ng-init="b({!! Auth::user()->id !!})"><%company%></p>
					</div> -->
					<div>
						<img src="{!!asset('ec/template/images/icon/png/mail.png')!!}" alt="">
						<p class="d-inline-block">{!! Auth::user()->email !!}</p>
					</div>
				</div><hr>
				<div class="text-center">
					<!-- <button class="btn btn-outline-info" data-toggle="modal" data-target="#myModal">Change E-mail</button> -->
					<button class="btn btn-outline-info" data-toggle="modal" data-target="#myModal">Change Password</button>
				</div>				
				<div id="popup">
					<!-- Modal -->
					<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog" role="document">
					    <div class="modal-content">
					      <div class="modal-header">
					        <h5 class="modal-title" id="myModalLabel">Modal title</h5>
					        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
					          <span aria-hidden="true">&times;</span>
					        </button>
					      </div>
					      <div class="modal-body">
					        <!-- <form>
							  <div class="form-group row">
							    <label for="staticEmail" class="col-md 4 col-form-label">Current Email</label>
							    <div class="col-md-8 pull-1">
							      <input type="text" readonly class="form-control-plaintext" id="staticEmail" value="{!! Auth::user()->email !!}">
							    </div>
							  </div>
							  <div class="form-group row">
							    <label for="inputEmail" class="col-md-4 col-form-label">New Email</label>
							    <div class="col-md-8 pull-1">
							      <input type="email" class="form-control" id="inputEmail">
							    </div>
							  </div>
							</form> -->
							<form name="myForm" method='post'>
							  <div class="form-group row">
							    <label for="inputEmail" class="col-md-4 col-form-label">New Pwd</label>
							    <div class="col-md-8 pull-1">
							      <input type="Password" class="form-control" id="new_pwd" ng-minlength='5' ng-model="pass.new" required>
							      <small class="form-text text-danger" ng-hide="pass.new.length>ng-minlength">Your password must have at least 5 characters</small>
							    </div>
							  </div>
							  <div class="form-group row">
							    <label for="inputEmail" class="col-md-4 col-form-label">Re-type Pwd</label>
							    <div class="col-md-8 pull-1">
							      <input type="Password" class="form-control" id="re_pwd" ng-minlength='5' ng-model="pass.re" required>
							      <small class="form-text text-danger" ng-hide="pass.re.length>ng-minlength">Your password must have at least 5 characters</small>
							    </div>
							  </div>
							  <!--<small class="form-text text-danger" ng-show="myForm.$invalid">You must enter all fields</small>-->
							</form>
					      </div>
					      <div class="modal-footer">
					        <button type="button" class="btn btn-secondary" data-dismiss="modal" ng-click="reset()">Close</button>
					        <button type="button" class="btn btn-primary" ng-disabled="myForm.$invalid || pass.new!=pass.re" ng-click="send()">Save</button>
					        <!-- <code>myForm = <%myForm.$invalid%></code> -->
					      </div>
					    </div>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</div>  
</div>
@endsection