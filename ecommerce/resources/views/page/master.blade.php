<!DOCTYPE html>
<html ng-app="my-app">

<head>
  <meta http-equiv="content-type" content="text/html;charset=UTF-8">
  <!-- /Added by HTTrack -->
  <meta charset="utf-8">
  <!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<![endif]-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Accord, premium HTML5 &amp; CSS3 template</title>
  <!-- Favicons Icon -->
  {{-- <link rel="icon" href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico" type="image/x-icon">
  <link rel="shortcut icon" href="http://demo.magikthemes.com/skin/frontend/base/default/favicon.ico" type="image/x-icon"> --}}
  <!-- Mobile Specific -->
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <!-- CSS Style -->
  <!--<link rel="stylesheet" href="css/animate.css" type="text/css">-->
  <link rel="stylesheet" href="{!!asset('css/bootstrap.min.css')!!}" type="text/css">
  <link rel="stylesheet" href="{!!asset('css/slider.css')!!}" type="text/css">
  <link rel="stylesheet" href="{!!asset('css/owl.carousel.css')!!}" type="text/css">
  <link rel="stylesheet" href="{!!asset('css/owl.theme.css')!!}" type="text/css">
  <link rel="stylesheet" href="{!!asset('css/font-awesome.css')!!}" type="text/css">
  <link rel="stylesheet" href="{!!asset('css/style.css')!!}" type="text/css">
  
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Bitter:400,700,400italic" rel="stylesheet" type="text/css"> </head>

<body class="cms-index-index">
  <div class="page">
    <!-- Header -->
    @include('page.main.header')

    <!-- end header -->
    <!-- Navbar -->
    @include('page.main.menu')

    <!-- end nav -->
    
    @yield ('content')
   
    <!-- Footer -->
   @include('page.main.footer')
    <!-- End Footer -->
  </div>
 @include('page.main.helpsite')
  
  <!-- JavaScript -->
  <script type="text/javascript" src="{!!asset('js/jquery.min.js')!!}"></script>
  <script type="text/javascript" src="{!!asset('js/bootstrap.min.js')!!}"></script>
  <script type="text/javascript" src="{!!asset('js/parallax.js')!!}"></script>
  <script type="text/javascript" src="{!!asset('js/common.js')!!}"></script>
  <script type="text/javascript" src="{!!asset('js/slider.js')!!}"></script>
  <script type="text/javascript" src="{!!asset('js/owl.carousel.min.js')!!}"></script>
  <script type="text/javascript" src="{!!asset('ec/template/js/angular.min.js')!!}"></script>
  {{-- <script type="text/javascript" src="{!!asset('js/angular-route.min.js')!!}"></script> --}}
  <script type="text/javascript" src="{!!asset('js/angular-pusher.min.js')!!}"></script>
  <script type="text/javascript" src="{!!asset('ec/template/js/index.js')!!}"></script>
  
  <script defer src="{!!asset('js/dirPagination.js')!!}"></script>
 
  <script type="text/javascript">

    //<![CDATA[
	jQuery(function() {
		jQuery(".slideshow").cycle({
			fx: 'scrollHorz', easing: 'easeInOutCubic', timeout: 10000, speedOut: 800, speedIn: 800, sync: 1, pause: 1, fit: 0, 			pager: '#home-slides-pager',
			prev: '#home-slides-prev',
			next: '#home-slides-next'
		});
	});
    //]]>
  </script>
 
</body>

</html>