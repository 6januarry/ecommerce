@extends('page.master')
 @section('content')  
   <div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <ul>
            <li class="home">
              <a href="index.html" title="Go to Home Page">Home</a><span>»</span></li>
            <li class="">
              <a href="grid.html" title="Go to Home Page">Women</a><span>»</span></li>
            <li class="category13"><strong> Navi Dresses </strong></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- end breadcrumbs -->
    <!-- main-container -->
    <section class="main-container col1-layout" ng-controller="CoursesController">
      <div class="main container">
        <div class="col-main">
          <div class="row">
            <div class="product-view">
              <div class="product-essential">
                <form action="#" method="post" id="product_addtocart_form">
                  <input name="form_key" value="6UbXroakyQlbfQzK" type="hidden">
                  <div class="product-img-box col-sm-4 col-xs-12">
                    <div class="new-label new-top-left"> New </div>
                    <div class="product-image">
                      <div class="large-image">
                       
                          <img alt="product-image" src="{!!asset('{{$product_details->picture}}')!!}"> </a>
                      </div>
                      <div class="flexslider flexslider-thumb">
                        <ul class="previews-list slides">
                          <li>
                            <a href="{!!asset('{{$product_details->picture}}')!!}" class="cloud-zoom-gallery" rel="useZoom: 'zoom1', smallImage: '{{$product_details->picture}}' ">
                              <img src="{!!asset('{{$product_details->picture}}')!!}" alt="Thumbnail 1">
                            </a>
                          </li>
                          
                        </ul>
                      </div>
                    </div>
                    <!-- end: more-images -->
                    <div class="clear"></div>
                  </div>
                  <div class="product-shop col-sm-5 col-xs-12">
                    <div class="product-next-prev">
                      <a class="product-next" href="#"><span></span></a>
                      <a class="product-prev" href="#"><span></span></a>
                    </div>
                    <div class="product-name">
                      <h1>{{$product_details->product_name}}</h1>
                    </div>
                    <div class="short-description">
                      <p>{{$product_details->description}} </p>
                    </div>
                    <div class="ratings">
                      <div class="rating-box">
                        <div style="width:60%" class="rating"></div>
                      </div>
                      <p class="rating-links">
                        <a href="#">1 Review(s)</a> <span class="separator">|</span>
                        <a href="#">Add Your Review</a>
                      </p>
                    </div>
                    <p class="availability in-stock">Availability: <span>In Stock</span></p>
                    <div class="price-block">
                      <div class="price-box">
                       
                        <p class="special-price"> <span class="price-label">Special Price</span> <span class="price"> {{$product_details->price_unit}} </span> </p>
                      </div>
                    </div>
                    <div class="add-to-box">
                      <div class="add-to-cart"> <label>Qty : </label> <select>
                        <option>1</option>
                        <option>2 </option>
                        <option>3</option>
                        <option>4</option>
                        <option>5 </option>
                        <option>6</option>
                        <option>7</option>
                        <option>8 </option>
                        <option>9</option>
                        <option>10</option>
                      </select> </div>
                      <button type="button" title="Add to Cart" class="button btn-cart" onclick="productAddToCartForm.submit(this)"><span>Add to Cart</span></button>
                    </div>
                    <div class="email-addto-box">
                      <p class="email-friend">
                        <a href="#" data-toggle="tooltip" data-placement="top" data-original-title="Email to a Friend"><span></span></a>
                      </p>
                    
                    </div>
                  
                  </div>
                  
                </form>
                <!-- related Product Slider -->
                
              </div>
            </div>
           
          </div>
        </div>
      </div>
    </section>
    <!-- End Two columns content -->
   
    </div>
    @endsection