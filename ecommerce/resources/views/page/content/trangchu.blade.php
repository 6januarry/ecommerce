 @extends('page.master')
 @section('content') 


        <!-- Slider -->
    <section class="slider-intro" >
      <div class="container">
        <div class="the-slideshow slideshow-wrapper">
          <ul style="overflow: hidden;" class="slideshow">
            <li class="slide">
              <p>
                <a href="#">
                  <img src="images/banner-img.jpg" alt="">
                </a>
              </p>
              <div class="caption light1 top-right">
                <div class="caption-inner">
                  <p class="heading"> Collection 2017</p>
                  <div class="badge"><em>up to</em> 70% <span>OFF</span></div>
                  {{-- <p class="heading1">Lorem Ipsum is simply dummy text of the printing and typesetting industry it has been the industry's standard dummy</p> --}}
                  <p class="intro-btn">
                    <a href="#" title="Click to see all features">Shop Now</a>
                  </p>
                </div>
              </div>
            </li>
            <li class="slide">
              <p>
                <a href="#">
                  <img src="images/banner-img1.jpg" alt="">
                </a>
              </p>
              <div class="caption light top-right">
                <div class="caption-inner">
                  <p class="normal-text">Collection 2017</p>
                  <h2 class="heading permanent">In This Autumn</h2>
                 {{--  <p class="normal-text1">tristique senectus </p> --}}
                  <p class="intro-btn">
                    <a href="#" title="Click to see all features">Buy Now</a>
                  </p>
                </div>
              </div>
            </li>
            <li class="slide">
              <p>
                <a title="#" href="#">
                  <img src="images/banner-img2.jpg" alt=""> </a>
              </p>
              <div class="caption light2 top-right">
                <div class="caption-inner">
                  {{-- <p class="heading">Responsive Layout</p> --}}
                </div>
              </div>
            </li>
          </ul>
          <a href="#" id="home-slides-prev" class="backward browse-button">previous</a>
          <a href="#" id="home-slides-next" class="forward browse-button">next</a>
          <div id="home-slides-pager" class="tab-pager tab-pager-img tab-pager-ring-lgray"></div>
        </div>
      </div>
    </section>
    <!-- end Slider -->
    <!-- service section  -->
    <div class="service-section" >
      <div class="container">
        <div id="store-messages">
         
          <div class="message col-lg-3 col-sm-6 col-xs-12"><i class="icon-truck">&nbsp;</i><span><strong>Free Shipping</strong> order over $99</span> </div>
          <div class="message col-lg-3 col-sm-6 col-xs-12"><i class="icon-discount">&nbsp;</i><span><strong>50% off</strong> on all products</span> </div>
          <div class="phone col-lg-3 col-sm-6 col-xs-12"><i class="icon-phone"></i>&nbsp;<span><strong>Need help?</strong> +84 1215 2368 28</span> </div>
        </div>
      </div>
    </div>
    <!-- End service section  -->
    <!-- Offer Banner  -->
    <div class="offer-banner-section" >
      <div class="container">
        <div class="col-1">
          <img src="images/offer-banner1.png" alt="offer banner">
        </div>
        <div class="col-2">
          <div class="add-banner">
            <img src="images/offer-banner2.png" alt="offer banner">
          </div>
        </div>
        <div class="col-3">
          <div class="add-banner1">
            <div class="add-banner2">
              <img src="images/offer-banner3.png" alt="offer banner">
            </div>
            <div class="add-banner3">
              <img src="images/offer-banner4.png" alt="offer banner">
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Offer Banner  -->
    <!-- main container -->
    <section class="main-container col1-layout home-content-container"  >
      <div class="container" ng-controller='CoursesController'>
        <div class="row">
          <div class="std">
            <div class="best-pro col-lg-12">
              <div class="slider-items-products">
                <div class="new_title center">
                  <h2>Best sellers</h2>
                </div>
                <div id="best-seller-slider" class="product-flexslider hidden-buttons">
                  <div class="slider-items slider-width-col4"  >
                    <!-- Item -->
                    <data-owl-carousel class="owl-carousel">
                      <div owl-carousel-item="" ng-repeat="bestseller in BestSellers" class="item">
                        <div class="col-item"  >
                        <div class="item-inner" >
                          <div class="item-img">
                           
                            <div class="item-img-info">
                              <a href="" title="Sample Product" class="product-image" ng-click="product_details(bestseller.id)">
                                <img src="<% bestseller.picture %>" alt="Sample Product"> </a>
                              <div class="item-box-hover">
                                <div class="box-inner">
                                  <div class="product-detail-bnt">
                                    <a href="{!! route('quick_view') !!}" class="button detail-bnt"> <span> Quick View</span></a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="item-info">
                            <div class="info-inner">
                              <div class="item-title">
                                <a href="" title="Sample Product" ng-click="product_details(bestseller.id)">  <% bestseller.product_name %>
                                </a>
                              </div>
                              <div class="item-content">
                               
                                <div class="item-price">
                                  <div class="price-box"> <span class="regular-price" id="product-price-1"> <span class="price"><% bestseller.price_unit %> VND</span> </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="actions"><span class="add-to-links"> 
                              <button title="Add to Cart" class="button btn-cart"><span>Add to Cart</span></button>
                              
                              </span>
                            </div>
                          </div>
                        </div>
                        </div>
                      </div >
                    </data-owl-carousel>
                  
                </div>
              </div>
            </div>
            <div class="featured-pro col-lg-12">
              <div class="slider-items-products">
                <div class="new_title center">
                  <h2>Featured Products</h2>
                </div>
                <div id="best-seller-slider" class="product-flexslider hidden-buttons">
                  <div class="slider-items slider-width-col4"  >
                    <!-- Item -->
                    <data-owl-carousel class="owl-carousel">
                      <div owl-carousel-item="" ng-repeat="product in Products" class="item">
                        <div class="col-item"  >
                        <div class="item-inner" >
                          <div class="item-img">
                           
                            <div class="item-img-info">
                              <a href="" title="Sample Product" class="product-image" ng-click="product_details(product.id)">
                                <img src=" <% product.picture %>" alt="Sample Product"> </a>
                              <div class="item-box-hover">
                                <div class="box-inner">
                                  <div class="product-detail-bnt">
                                    <a href="{!! route('quick_view') !!}" class="button detail-bnt"> <span> Quick View</span></a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="item-info">
                            <div class="info-inner">
                              <div class="item-title">
                                <a href="" title="Sample Product" ng-click="product_details(product.id)">  <% product.product_name %>
                                </a>
                              </div>
                              <div class="item-content">
                                
                                <div class="item-price">
                                  <div class="price-box"> <span class="regular-price" id="product-price-1"> <span class="price"><% product.price_unit %> VND</span> </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="actions"><span class="add-to-links"> 
                              <button title="Add to Cart" class="button btn-cart"><span>Add to Cart</span></button>
                              
                              </span>
                            </div>
                          </div>
                        </div>
                        </div>
                      </div >
                    </data-owl-carousel>
                  
                </div>
              </div>
            </div>
            
                                   
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </section>
    <!-- End main container -->
    

    </div>
  @endsection