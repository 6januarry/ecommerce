 @extends('page.master')
 @section('content')  
<div class="main-container col2-right-layout">
    <div class="main container" ng-controller='CoursesController'>
      <div class="row">
        <section class="col-main col-sm-9 col-sm-push-3">
          <div class="category-title">
            <h1>Tops &amp; Tees</h1>
          </div>
          <div class="category-description std">
            <div class="category-image"><img src="images/women_banner.png" alt="cat imges " title="Sofas "> </div>
          </div>
          <div class="category-products">
            <div class="toolbar">
              <div class="sorter">
                <div class="view-mode"> <a class="button button-grid" title="Grid" href="{!! route('grid') !!}">Grid</a>&nbsp; <span class="button button-active button-list" title="List">List</span>&nbsp; </div>
              </div>
              <div id="sort-by">
                <label class="left">Sort By: </label>
                <ul>
                  <li><a href="#">Position<span class="right-arrow"></span></a>
                    <ul>
                      <li><a href="#">Name</a></li>
                      <li><a href="#">Price</a></li>
                      <li><a href="#">Position</a></li>
                    </ul>
                  </li>
                </ul>
                <a title="Set Descending Direction" href="#?dir=desc&amp;order=position" class="button-asc left"><span class="glyphicon glyphicon-arrow-up" style="color:#999;font-size:11px;"></span></a> </div>
              <div class="pager">
                <div id="limiter">
                  <label>View: </label>
                  <ul>
                    <li><a href="#">15<span class="right-arrow"></span></a>
                      <ul>
                        <li><a href="#">20</a></li>
                        <li><a href="#">30</a></li>
                        <li><a href="#">35</a></li>
                      </ul>
                    </li>
                  </ul>
                </div>
                <div class="pages">
                  <label>Page:</label>
                  <ul class="pagination">
                    <li><a href="#">«</a></li>
                    <li class="active"><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">»</a></li>
                  </ul>
                </div>
              </div>
            </div>
            <ol id="products-list" class="products-list">
              <li class="item" ng-repeat="product in Products">
                <div class="col-item">
                  <div class="product-image"> <a href="{!! route('product_detail') !!}" title="Sample Product"> <img class="small-image" src="<% product.link %>" alt="HTC Rhyme Sense"> </a> </div>
                  <div class="product-shop">
                    <h2 class="product-name"><a title=" Sample Product" href="{!! route('product_detail') !!}"><% product.product_name %>  </a></h2>
                    <div class="price-box">
                    {{--   <p class="old-price"> <span class="price-label"></span> <span id="old-price-212" class="price"> $442.99 </span> </p> --}}
                      <p class="special-price"> <span class="price-label"></span> <span id="product-price-212" class="price"> <% product.price_unit %> VND </span> </p>
                    </div>
                    <div class="ratings">
                      <div class="rating-box">
                        <div style="width:50%" class="rating"></div>
                      </div>
                      <p class="rating-links"> <a href="#">1 Review(s)</a> <span class="separator">|</span> <a href="#review-form">Add Your Review</a> </p>
                    </div>
                    <div class="desc std">
                      
                      <p><% product.description %> <a class="link-learn" title="" href="#">Learn More</a> </p>
                    </div>
                    <div class="actions">
                      <button class="button btn-cart ajx-cart" title="Add to Cart" type="button"><span>Add to Cart</span></button>
                      <span class="add-to-links">  </span> </div>
                  </div>
                </div>
              </li>
              
            </ol>
          </div>
        </section>
        @include('page.content.product-menu.product_sidebar') 
      </div>
    </div>
  </div>
  @endsection