   @extends('page.master')
 @section('content')  
 <div class="breadcrumbs">
      <div class="container">
        <div class="row">
          <ul>
            <li class="home">
              <a href="{!! route('index_page') !!}" title="Go to Home Page">Home</a><span>» </span></li>
            <li class="">
              <a href="{!! route('grid') !!}" title="Go to Home Page">Women</a><span>» </span></li>
            <li class="category13"><strong>Tops &amp; Tees</strong></li>
          </ul>
        </div>
      </div>
    </div>
    <!-- End breadcrumbs -->
    <!-- Two columns content -->
    <div class="main-container col2-left-layout" ng-controller='CoursesController'>
      <div class="main container">
        <div class="row">
          <section class="col-main col-sm-9 col-sm-push-3">
            <div class="category-title">
              <h1>Tops &amp; Tees</h1>
            </div>
          @include('page.content.product-menu.category_description')  
            <div class="category-products">
              <div class="toolbar">
                <div class="sorter">
                  <div class="view-mode"> <span class="button button-active button-grid" title="Grid">Grid</span>
                    <a class="button button-list" title="List" href="{!! route('list') !!}">List</a>
                  </div>
                </div>
                <div id="sort-by"> <label class="left">Sort By: </label>
                  <ul>
                    <li>
                      <a href="#">Position<span class="right-arrow"></span></a>
                      <ul>
                        <li>
                          <a href="#">Name</a>
                        </li>
                        <li>
                          <a href="#">Price</a>
                        </li>
                        <li>
                          <a href="#">Position</a>
                        </li>
                      </ul>
                    </li>
                  </ul>
                  <a title="Set Descending Direction" href="#" class="button-asc left"><span class="glyphicon glyphicon-arrow-up" style="color:#999;font-size:11px;"></span></a>
                </div>
                <div class="pager">
                  <div class="pages"> <label>Page:</label>
                    <ul class="pagination">
                      <li>
                        <a href="#">«</a>
                      </li>
                      <li class="active">
                        <a href="#">1</a>
                      </li>
                      <li>
                        <a href="#">2</a>
                      </li>
                      <li>
                        <a href="#">3</a>
                      </li>
                      <li>
                        <a href="#">4</a>
                      </li>
                      <li>
                        <a href="#">5</a>
                      </li>
                      <li>
                        <a href="#">»</a>
                      </li>
                    </ul>
                  </div>
                  <div id="limiter"> <label>View: </label>
                    <ul>
                      <li>
                        <a href="#">15<span class="right-arrow"></span></a>
                        <ul>
                          <li>
                            <a href="#">20</a>
                          </li>
                          <li>
                            <a href="#">30</a>
                          </li>
                          <li>
                            <a href="#">35</a>
                          </li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <ul class="products-grid">
                <li class="item col-lg-4 col-md-4 col-sm-6 col-xs-12" ng-repeat="product in Products">
                  <div class="col-item">
                    <div class="sale-label sale-top-right">Sale</div>
                    <div class="item-inner">
                      <div class="item-img">
                        <div class="item-img-info">
                          <a  title="Sample Product" class="product-image" ng-click="product_details(product.id)">
                            <img src="<% product.picture %>" alt="Ut tincidunt tortor">
                          </a>
                          <div class="item-box-hover">
                            <div class="box-inner">
                              <div class="product-detail-bnt">
                                <a href="{!! route('quick_view') !!}" class="button detail-bnt"><span>Quick View</span></a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="item-info">
                        <div class="info-inner">
                          <div class="item-title">
                            <a href="" title="Sample Product" ng-click="product_details(product.id)"> <% product.product_name %> </a>
                          </div>
                          <div class="item-content">
                            <div class="rating">
                              <div class="ratings">
                                <div class="rating-box">
                                  <div class="rating" style="width:60%"></div>
                                </div>
                                
                                </p>
                              </div>
                            </div>
                            <div class="item-price">
                              <div class="price-box"> <span class="regular-price"> <span class="price"><% product.price_unit %> VND</span> </span>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="actions"><span class="add-to-links"> 
                          <button type="button" title="Add to Cart" class="button btn-cart"><span>Add to Cart</span></button>
                        
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>
                </li>
    
              </ul>
            </div>
          </section>
         @include('page.content.product-menu.product_sidebar') 
        </div>
      </div>
    </div>
    @endsection