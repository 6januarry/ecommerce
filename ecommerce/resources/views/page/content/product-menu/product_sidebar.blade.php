 <aside class="col-right sidebar col-sm-3 col-xs-12 col-sm-pull-9">
            <div class="side-nav-categories">
              <div class="block-title"> Categories </div>
              <!--block-title-->
              <!-- BEGIN BOX-CATEGORY -->
              <div class="box-content box-category">
                <ul>
                  <li>
                    <a class="active" href="#/women.html">Women</a> <span class="subDropdown minus"></span>
                    <ul class="level0_415" style="display:block">
                      <li>
                        <a href="{!! route('grid') !!}"> Tops </a> <span class="subDropdown plus"></span>
                        <ul class="level1" style="display:none">
                          <li>
                            <a href="{!! route('grid') !!}"> Evening Tops </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Shirts &amp; Blouses </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Tunics </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Vests </a>
                          </li>
                          <!--end for-each -->
                        </ul>
                        <!--level1-->
                      </li>
                      <!--level1-->
                      <li>
                        <a href="{!! route('grid') !!}"> Bags </a> <span class="subDropdown plus"></span>
                        <ul class="level1" style="display:none">
                          <li>
                            <a href="{!! route('grid') !!}"> Bags </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Designer Handbags </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Purses </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Shoulder Bags </a>
                          </li>
                          <!--end for-each -->
                        </ul>
                        <!--level1-->
                      </li>
                      <!--level1-->
                      <li>
                        <a href="{!! route('grid') !!}"> Shoes </a> <span class="subDropdown plus"></span>
                        <ul class="level1" style="display:none">
                          <li>
                            <a href="{!! route('grid') !!}"> Flat Shoes </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Flat Sandals </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Boots </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Heels </a>
                          </li>
                          <!--end for-each -->
                        </ul>
                        <!--level1-->
                      </li>
                      <!--level1-->
                      <li>
                        <a href="{!! route('grid') !!}"> Jewellery </a>
                        <ul class="level1" style="display:none">
                          <li>
                            <a href="{!! route('grid') !!}"> Bracelets </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Necklaces &amp; Pendants </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Pins &amp; Brooches </a>
                          </li>
                          <!--end for-each -->
                        </ul>
                        <!--level1-->
                      </li>
                      <!--level1-->
                      <li>
                        <a href="{!! route('grid') !!}"> Dresses </a> <span class="subDropdown plus"></span>
                        <ul class="level1" style="display:none">
                          <li>
                            <a href="{!! route('grid') !!}"> Casual Dresses </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Evening </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Designer </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Party </a>
                          </li>
                          <!--end for-each -->
                        </ul>
                        <!--level1-->
                      </li>
                      <!--level1-->
                      <li>
                        <a href="{!! route('grid') !!}"> Lingerie </a> <span class="subDropdown plus"></span>
                        <ul class="level1" style="display:none">
                          <li>
                            <a href="{!! route('grid') !!}"> Bras </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Bodies </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Lingerie Sets </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Shapewear </a>
                          </li>
                          <!--end for-each -->
                        </ul>
                        <!--level1-->
                      </li>
                      <!--level1-->
                      <li>
                        <a href="{!! route('grid') !!}"> Jackets </a> <span class="subDropdown plus"></span>
                        <ul class="level1" style="display:none">
                          <li>
                            <a href="{!! route('grid') !!}"> Coats </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Jackets </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Leather Jackets </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Blazers </a>
                          </li>
                          <!--end for-each -->
                        </ul>
                        <!--level1-->
                      </li>
                      <!--level1-->
                      <li>
                        <a href="{!! route('grid') !!}"> Swimwear </a> <span class="subDropdown plus"></span>
                        <ul class="level1" style="display:none">
                          <li>
                            <a href="{!! route('grid') !!}"> Swimsuits </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Beach Clothing </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Bikinis </a>
                          </li>
                          <!--end for-each -->
                        </ul>
                        <!--level1-->
                      </li>
                      <!--level1-->
                    </ul>
                    <!--level0-->
                  </li>
                  <!--level 0-->
                  <li>
                    <a href="{!! route('grid') !!}">Men</a> <span class="subDropdown plus"></span>
                    <ul class="level0_455" style="display:none">
                      <li>
                        <a href="#/men/shoes.html"> Shoes </a> <span class="subDropdown plus"></span>
                        <ul class="level1" style="display:none">
                          <li>
                            <a href="{!! route('grid') !!}"> Flat Shoes </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Boots </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Heels </a>
                          </li>
                          <!--end for-each -->
                        </ul>
                        <!--level1-->
                      </li>
                      <!--level1-->
                      <li>
                        <a href="{!! route('grid') !!}"> Jewellery </a> <span class="subDropdown plus"></span>
                        <ul class="level1" style="display:none">
                          <li>
                            <a href="{!! route('grid') !!}"> Bracelets </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Necklaces &amp; Pendants </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Pins &amp; Brooches </a>
                          </li>
                          <!--end for-each -->
                        </ul>
                        <!--level1-->
                      </li>
                      <!--level1-->
                      <li>
                        <a href="{!! route('grid') !!}"> Dresses </a> <span class="subDropdown plus"></span>
                        <ul class="level1" style="display:none">
                          <li>
                            <a href="{!! route('grid') !!}"> Casual Dresses </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Evening </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Designer </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Party </a>
                          </li>
                          <!--end for-each -->
                        </ul>
                        <!--level1-->
                      </li>
                      <!--level1-->
                      <li>
                        <a href="{!! route('grid') !!}"> Jackets </a> <span class="subDropdown plus"></span>
                        <ul class="level1" style="display:none">
                          <li>
                            <a href="{!! route('grid') !!}"> Coats </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Jackets </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Leather Jackets </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Blazers </a>
                          </li>
                          <!--end for-each -->
                        </ul>
                        <!--level1-->
                      </li>
                      <!--level1-->
                      <li>
                        <a href="{!! route('grid') !!}"> Swimwear </a> <span class="subDropdown plus"></span>
                        <ul class="level1" style="display:none">
                          <li>
                            <a href="{!! route('grid') !!}"> Swimsuits </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Beach Clothing </a>
                          </li>
                          <!--end for-each -->
                        </ul>
                        <!--level1-->
                      </li>
                      <!--level1-->
                    </ul>
                    <!--level0-->
                  </li>
                  <!--level 0-->
                  <li>
                    <a href="{!! route('grid') !!}">Electronics</a> <span class="subDropdown plus"></span>
                    <ul class="level0_482" style="display:none">
                      <li>
                        <a href="{!! route('grid') !!}"> Smartphones </a> <span class="subDropdown plus"></span>
                        <ul class="level1" style="display:none">
                          <li>
                            <a href="{!! route('grid') !!}"> Samsung </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Apple </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Blackberry </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Nokia </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> HTC </a>
                          </li>
                          <!--end for-each -->
                        </ul>
                        <!--level1-->
                      </li>
                      <!--level1-->
                      <li>
                        <a href="{!! route('grid') !!}"> Cameras </a> <span class="subDropdown plus"></span>
                        <ul class="level1" style="display:none">
                          <li>
                            <a href="{!! route('grid') !!}"> Digital Cameras </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Camcorders </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Lenses </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Filters </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Tripod </a>
                          </li>
                          <!--end for-each -->
                        </ul>
                        <!--level1-->
                      </li>
                      <!--level1-->
                      <li>
                        <a href="{!! route('grid') !!}"> Accesories </a> <span class="subDropdown plus"></span>
                        <ul class="level1" style="display:none">
                          <li>
                            <a href="{!! route('grid') !!}"> HeadSets </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Batteries </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Screen Protectors </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Memory Cards </a>
                          </li>
                          <li>
                            <a href="{!! route('grid') !!}"> Cases </a>
                          </li>
                          <!--end for-each -->
                        </ul>
                        <!--level1-->
                      </li>
                      <!--level1-->
                    </ul>
                    <!--level0-->
                  </li>
                  <!--level 0-->
                  <li>
                    <a href="{!! route('grid') !!}">Digital</a>
                  </li>
                  <!--level 0-->
                  <li class="last">
                    <a href="{!! route('grid') !!}">Fashion</a>
                  </li>
                  <!--level 0-->
                </ul>
              </div>
              <!--box-content box-category-->
            </div>
            <div class="block block-layered-nav">
              <div class="block-title">Shop By</div>
              <div class="block-content">
                <p class="block-subtitle">Shopping Options</p>
                <dl id="narrow-by-list"> <dt class="odd">Price</dt>
                  <dd class="odd">
                    <ol>
                      <li>
                        <a href="#"><span class="price">$0.00</span> - <span class="price">$99.99</span></a> (6) </li>
                      <li>
                        <a href="#"><span class="price">$100.00</span> and above</a> (6) </li>
                    </ol>
                  </dd> <dt class="even">Manufacturer</dt>
                  <dd class="even">
                    <ol>
                      <li>
                        <a href="#">TheBrand</a> (9) </li>
                      <li>
                        <a href="#">Company</a> (4) </li>
                      <li>
                        <a href="#">LogoFashion</a> (1) </li>
                    </ol>
                  </dd> <dt class="odd">Color</dt>
                  <dd class="odd">
                    <ol>
                      <li>
                        <a href="#">Green</a> (1) </li>
                      <li>
                        <a href="#">White</a> (5) </li>
                      <li>
                        <a href="#">Black</a> (5) </li>
                      <li>
                        <a href="#">Gray</a> (4) </li>
                      <li>
                        <a href="#">Dark Gray</a> (3) </li>
                      <li>
                        <a href="#">Blue</a> (1) </li>
                    </ol>
                  </dd> <dt class="last even">Size</dt>
                  <dd class="last even">
                    <ol>
                      <li>
                        <a href="#">S</a> (6) </li>
                      <li>
                        <a href="#">M</a> (6) </li>
                      <li>
                        <a href="#">L</a> (4) </li>
                      <li>
                        <a href="#">XL</a> (4) </li>
                    </ol>
                  </dd>
                </dl>
              </div>
            </div>
            <div class="block block-subscribe">
              <div class="block-title">Newsletter Sign Up</div>
              <form action="http://www.magikcommerce.com//newsletter/subscriber/new/" method="post" id="newsletter-validate-detail">
                <div class="block-content">
                  <div class="form-subscribe-header"> Sign up for our newsletter:</div>
                  <input type="text" name="email" id="newsletter" title="Sign up for our newsletter" class="input-text required-entry validate-email" placeholder="Enter your email address">
                  <div class="actions">
                    <button class="button button-subscribe" title="Submit" type="submit"><span>Subscribe</span></button>
                  </div>
                </div>
              </form>
            </div>
            <div class="block block-cart">
              <div class="block-title ">My Cart</div>
              <div class="block-content">
                <div class="summary">
                  <p class="amount">There are
                    <a href="#">2 items</a> in your cart.</p>
                  <p class="subtotal"> <span class="label">Cart Subtotal:</span> <span class="price">$27.99</span> </p>
                </div>
                <div class="ajax-checkout">
                  <button type="submit" title="Submit" class="button button-checkout"><span>Checkout</span></button>
                </div>
                <p class="block-subtitle">Recently added item(s) </p>
                <ul>
                  <li class="item">
                    <a class="product-image" title="Fisher-Price Bubble Mower" href="product_detail.html">
                      <img width="60" alt="Fisher-Price Bubble Mower" src="products-images/product1.jpg">
                    </a>
                    <div class="product-details">
                      <div class="access">
                        <a class="btn-remove1" title="Remove This Item" href="#"> <span class="icon"></span> Remove </a>
                      </div>
                      <p class="product-name">
                        <a href="product_detail.html">Sample Product</a>
                      </p> <strong>1</strong> x <span class="price">$19.99</span> </div>
                  </li>
                  <li class="item last">
                    <a class="product-image" title="Prince Lionheart Jumbo Toy Hammock" href="product_detail.html">
                      <img width="60" alt="Prince Lionheart Jumbo Toy Hammock" src="products-images/product2.jpg">
                    </a>
                    <div class="product-details">
                      <div class="access">
                        <a class="btn-remove1" title="Remove This Item" href="#"> <span class="icon"></span> Remove </a>
                      </div>
                      <p class="product-name">
                        <a href="product_detail.html"> Sample Product </a>
                      </p> <strong>1</strong> x <span class="price">$8.00</span>
                      <!--access clearfix-->
                    </div>
                  </li>
                </ul>
              </div>
            </div>
          
           
            
            <div class="block block-banner">
              <a href="#">
                <img src="images/block-banner.png" alt="">
              </a>
            </div>
          </aside>