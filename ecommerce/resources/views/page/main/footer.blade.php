<footer class="footer">
      <div class="footer-middle">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
              <div class="footer-logo">
                <a href="index.html" title="Logo">
                  <img src="{!!asset('images/logo.png')!!}" alt="logo">
                </a>
              </div> <address>
            <i class="add-icon">&nbsp;</i>83 Lach Tray Str <br>
            &nbsp;Hai Phong, Viet Nam
            </address>
              <div class="phone-footer"><i class="phone-icon">&nbsp;</i> +84 1215 2368 28</div>
              <div class="email-footer"><i class="email-icon">&nbsp;</i>
                <a href="mailto:h2ncompany1195@gmail.com">h2ncompany1195@gmail.com</a>
              </div>
            </div>
          
           
            <div class="col-lg-3 col-md-8 col-sm-9 col-xs-12">
              <div class="block-subscribe">
                <h4>Sign up for emails</h4>
                <div class="newsletter">
                  <form>
                    <input type="text" placeholder="Enter your email" class="input-text required-entry validate-email" title="Sign up for our newsletter" id="newsletter1" name="email">
                    <button class="subscribe" title="Subscribe" type="submit"><span>Submit</span></button>
                  </form>
                </div>
              </div>
              <div class="social">
                <h4>Follow Us</h4>
                <ul>
                  <li class="fb">
                    <a href="#"></a>
                  </li>
                  <li class="tw">
                    <a href="#"></a>
                  </li>
                  <li class="googleplus">
                    <a href="#"></a>
                  </li>
                  <li class="rss">
                    <a href="#"></a>
                  </li>
                  <li class="pintrest">
                    <a href="#"></a>
                  </li>
                  <li class="linkedin">
                    <a href="#"></a>
                  </li>
                  <li class="youtube">
                    <a href="#"></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-bottom">
        <div class="container">
          <div class="row">
            <div class="col-sm-4 col-xs-12 coppyright"> © H2N Company. All Rights Reserved.</div>
            <div class="col-sm-8 col-xs-12 company-links">
              
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>