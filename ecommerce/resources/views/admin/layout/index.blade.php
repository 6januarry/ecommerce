@extends('admin.layout.master')
@section('content')
<div id="page-wrapper" ng-controller="controller">
	<div class="container-fluid">
		<div class="row">
		<!-- Sales stats -->
			<div class="col-lg-12">
		        <h1 class="page-header">Sales 
		            <small>Statistics</small>
		        </h1>
		    </div>
			<div class="container-fluid">
				<div class="row text-center">
					<div class="col-md-4">
						<div class="content-group">
							<h4 class="text-semibold no-margin"><i class="icon-calendar5 position-left text-slate"></i> <%orderstoday%></h4>
							<span class="text-muted text-size-small">orders today</span>
						</div>
					</div>

					<div class="col-md-4">
						<div class="content-group">
							<h4 class="text-semibold no-margin"><i class="icon-calendar52 position-left text-slate"></i> <%ordersmonthly%></h4>
							<span class="text-muted text-size-small">orders monthly</span>
						</div>
					</div>

					<div class="col-md-4">
						<div class="content-group">
							<h4 class="text-semibold no-margin"><i class="icon-cash3 position-left text-slate"></i> <%revenue%></h4>
							<span class="text-muted text-size-small">revenue monthly</span>
						</div>
					</div>
				</div>
			</div>
		<!-- /sales stats -->
		</div>
		<div class="row">
			<div class="col-lg-12">
		        <h1 class="page-header">Chart
		        </h1>
		        <div class="app">
		            <center>
		                {!! $chart->html() !!}
		            </center>
        		</div>
        		{!! Charts::scripts() !!}
        		{!! $chart->script() !!}
			</div>
		    </div>
		</div>
	</div>
</div>
@endsection('content')