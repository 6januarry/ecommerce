<!DOCTYPE html>
<html ng-app="myAdmin">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="author" content="">
    <title></title>

    <!-- Bootstrap Core CSS -->
    <link href="{!!asset('ec/template/AdminPanel_assets/bower_components/bootstrap/dist/css/bootstrap.min.css')!!}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{!!asset('ec/template/AdminPanel_assets/bower_components/metisMenu/dist/metisMenu.min.css')!!}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{!!asset('ec/template/AdminPanel_assets/dist/css/sb-admin-2.css')!!}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{!!asset('ec/template/AdminPanel_assets/bower_components/font-awesome/css/font-awesome.min.css')!!}" rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <link href="{!!asset('ec/template/AdminPanel_assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css')!!}" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="{!!asset('ec/template/AdminPanel_assets/bower_components/datatables-responsive/css/dataTables.responsive.css')!!}" rel="stylesheet">
    <!-- icon -->
    <link href="{!!asset('ec/template/AdminPanel_assets/bower_components/icons/icomoon/styles.css')!!}" rel="stylesheet" type="text/css">
    
    <meta name="_token" content="{{csrf_token()}}">
</head>

<body>

    <div id="wrapper">

    @include('admin.layout.header')
       

    @yield('content')

    </div>

    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{!!asset('ec/template/AdminPanel_assets/bower_components/jquery/dist/jquery.min.js')!!}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{!!asset('ec/template/AdminPanel_assets/bower_components/bootstrap/dist/js/bootstrap.min.js')!!}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{!!asset('ec/template/AdminPanel_assets/bower_components/metisMenu/dist/metisMenu.min.js')!!}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{!!asset('ec/template/AdminPanel_assets/dist/js/sb-admin-2.js')!!}"></script>

    <!-- DataTables JavaScript -->
    <script src="{!!asset('ec/template/AdminPanel_assets/bower_components/DataTables/media/js/jquery.dataTables.min.js')!!}"></script>
    <script src="{!!asset('ec/template/AdminPanel_assets/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js')!!}"></script>
    <script src="{!!asset('ec/template/js/angular.min.js')!!}"></script>
    <script src="{!!asset('ec/template/js/angular-pusher.min.js')!!}"></script>
    <script type="text/javascript" src="{!!asset('ec/template/AdminPanel_assets/js/admin.js')!!}"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
</body>

</html>
