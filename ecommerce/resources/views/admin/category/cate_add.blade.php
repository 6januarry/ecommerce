 @extends('admin.layout.master')
 @section('content')

<div id="page-wrapper" ng-controller="controller2">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Category
                            <small>Add</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form method="post" name='formAdd'>
                            <div class="form-group">
                                <label>Category Parent</label>
                                <select class="form-control" ng-model="add.id_category">
                                    <option value="1">Clothing</option>
                                    <option value="2">Shoes</option>
                                    <option value="3">Accessories</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Category Name</label>
                                <input class="form-control" name="txtCateName" placeholder="Please Enter Category Name" ng-model="add.name" required/>
                            </div>
                            <!-- <div class="form-group">
                                <label>Category Order</label>
                                <input class="form-control" name="txtOrder" placeholder="Please Enter Category Order" />
                            </div>
                            <div class="form-group">
                                <label>Category Keywords</label>
                                <input class="form-control" name="txtOrder" placeholder="Please Enter Category Keywords" />
                            </div>
                            <div class="form-group">
                                <label>Category Description</label>
                                <textarea class="form-control" rows="3"></textarea>
                            </div> -->
                            <div class="form-group">
                                <label>Category Status</label>
                                <label class="radio-inline" >
                                    <input value="0" type="radio" ng-model="add.active">Visible
                                </label>
                                <label class="radio-inline">
                                    <input value="1" type="radio" ng-model="add.active">Invisible
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default" ng-click="addCate()" ng-disabled="formAdd.$invalid">Category Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
 @endsection