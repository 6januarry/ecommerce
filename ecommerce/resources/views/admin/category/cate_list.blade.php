 @extends('admin.layout.master')
 @section('content')
  <div id="page-wrapper" ng-controller='controller1'>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Category
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Category Parent</th>
                                <th>Status</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd gradeX" align="center" ng-repeat='category in categories'>
                                <td><%category.id%></td>
                                <td><%category.name%></td>
                                <td><%category.category_name%></td>
                                <td><p ng-show='category.active==1'>Hiện</p>
                                    <p ng-show='category.active==0'>Ẩn</p>
                                </td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="#" ng-click="confirmDel(category.id)">Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a style="cursor: pointer;" data-toggle="modal" data-target="#myModal" ng-click="edit(category.id)">Edit</a></td>
                            </tr>
                            <!-- data-toggle="modal" data-target="#myModal" style="cursor: pointer;" ng-click="modal('edit',s.id)" -->
                        </tbody>
                    </table>
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="myModalLabel">Category Edit</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="myForm.$setPristine()">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form name="myForm" method='post'>
                                    <div class="form-group">
                                        <label class="form-control-label" for="name">Category Parent</label>
                                        <select class="form-control" ng-model="cate_details.category_name">
                                            <option value="Clothing">Clothing</option>
                                            <option value="Shoes">Shoes</option>
                                            <option value="Accessories">Accessories</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label" >Category Name</label>
                                        <input type="text" class="form-control" id="cate_name" placeholder="Ex: Sneakers" ng-model="cate_details.name" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Active</label>
                                        <select class="form-control" ng-model="cate_details.active">
                                            <option value="0">Visible</option>
                                            <option value="1">Invisible</option>
                                        </select>
                                    </div>
                                </form>
                              </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" ng-click="myForm.$setPristine()">Close</button>
                            <button type="button" class="btn btn-primary" ng-disabled="myForm.$pristine || myForm.$invalid" ng-click="save(id)">Save</button>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
 @endsection