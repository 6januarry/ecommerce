 @extends('admin.layout.master')
 @section('content')
   <div id="page-wrapper" ng-controller="controller5">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">User
                            <small>Add</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                        <form name='formAdd'>
                            <div class="form-group">
                                <label>First Name</label>
                                <input class="form-control" name="txtUser" placeholder="Please Enter Username" ng-model="add.first_name" required/>
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input class="form-control" name="txtUser" placeholder="Please Enter Username" ng-model="add.last_name" required/>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" class="form-control" name="txtEmail" placeholder="Please Enter Email" ng-model="add.email" required/>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" name="txtPass" placeholder="Please Enter Password" ng-model="add.password" required ng-minlength="6"/>
                            </div>
                            <div class="form-group">
                                <label>RePassword</label>
                                <input type="password" class="form-control" name="txtRePass" placeholder="Please Enter RePassword" ng-model="check_password" required ng-minlength="6"/>
                            </div>
                            
                            <div class="form-group">
                                <label>User Level</label>
                                <label class="radio-inline">
                                    <input name="rdoLevel" value="0" checked="" type="radio" ng-model="add.level">Admin
                                </label>
                                <label class="radio-inline">
                                    <input name="rdoLevel" value="1" type="radio" ng-model="add.level">Deliver
                                </label>
                            </div>
                            <button type="submit" class="btn btn-default" ng-click="addUser()" ng-disabled="formAdd.$invalid">User Add</button>
                            <button type="reset" class="btn btn-default">Reset</button>
                        <form>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
 @endsection