 @extends('admin.layout.master')
 @section('content')
  <div id="page-wrapper" ng-controller='controller4'>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">User
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Username</th>
                                <th>Level</th>
                                <th>Status</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd gradeX" align="center" ng-repeat="user in users">
                                <td><%user.id%></td>
                                <td><%user.first_name%> <%user.last_name%></td>
                                <td>
                                    <p ng-show="user.level==0">Admin</p>
                                    <p ng-show="user.level==1">Deliver</p>
                                    <p ng-show="user.level==2">Member</p>
                                </td>
                                <td>
                                    <p ng-show="user.active==1">Active</p>
                                    <p ng-show="user.active==0">Inactive</p>
                                </td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><button ng-disabled="user.level==0" class='btn btn-primary' ng-click="confirmDel(user.id)">Delete</button></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <button ng-disabled="user.level==0" class='btn btn-primary' data-toggle="modal" data-target="#myModal" ng-click="edit(user.id)">Edit</button></td>
                            </tr>
                            <!-- <tr class="even gradeC" align="center">
                                <td>2</td>
                                <td>kutun</td>
                                <td>Admin</td>
                                <td>Ẩn</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="#"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="#">Edit</a></td>
                            </tr>
                            <tr class="odd gradeX" align="center">
                                <td>3</td>
                                <td>kuteo</td>
                                <td>Member</td>
                                <td>Hiện</td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="#"> Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="#">Edit</a></td>
                            </tr> -->
                        </tbody>
                    </table>
                    <pagination-User class="text-center"></pagination-User>
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="myModalLabel">User Edit</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="myForm.$setPristine()">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form name="myForm" method='post'>
                                    <div class="form-group">
                                        <label class="form-control-label" >User First Name</label>
                                        <input type="text" class="form-control" placeholder="Ex: Hai" ng-model="user_details.first_name" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label" >User Last Name</label>
                                        <input type="text" class="form-control" placeholder="Ex: Le" ng-model="user_details.last_name" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label" for="name">User Level</label>
                                        <select class="form-control" ng-model="user_details.level">
                                            <option value="1">Deliver</option>
                                            <option value="2">Member</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Active</label>
                                        <select class="form-control" ng-model="user_details.active">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>
                                </form>
                              </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" ng-click="myForm.$setPristine()">Close</button>
                            <button type="button" class="btn btn-primary" ng-disabled="myForm.$pristine || myForm.$invalid" ng-click="save(id)">Save</button>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
 @endsection