 @extends('admin.layout.master')
 @section('content')
     <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">User
                            <small>Information</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <div class="col-lg-7" style="padding-bottom:120px">
                            <div class="form-group">
                                <p><b>Name: </b>{!! Auth::user()->first_name !!} {!! Auth::user()->last_name !!}</p>
                            </div>
                            <div class="form-group">
                                <p><b>Email:</b> {!! Auth::user()->email !!}</p>
                            </div>
                            <div class="form-group">
                                <p><b>User Level:</b> Admin</p>
                            </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
 @endsection