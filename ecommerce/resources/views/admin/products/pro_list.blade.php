 @extends('admin.layout.master')
 @section('content')
  <div id="page-wrapper" ng-controller="controller3">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Product
                            <small>List</small>
                        </h1>
                    </div>
                    <!-- /.col-lg-12 -->
                    <form>
                        <select class="form-group" ng-model="product.cate" ng-change="change()">
                            <option value='1'>Clothing</option>
                            <option value='2'>Shoes</option>
                            <option value='3'>Accessories</option>
                        </select>
                        <select class="form-group" ng-model="id">
                            <option ng-repeat="option in product.cate_details" ng-value='option.id'><%option.name%></option>
                            <!-- <option value='2'>Shoes</option>
                            <option value='3'>Accessories</option> -->
                        </select>
                        <button class='btn btn-primary' ng-click="showProds()">Show</button>
                    </form>
                    <table class="table table-striped table-bordered table-hover" ng-hide="abc">
                        <thead>
                            <tr align="center">
                                <th>ID</th>
                                <th>Name</th>
                                <th>Description</th>
                                <th>Picture</th>
                                <th>Color</th>
                                <th>Size</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th>Discount</th>
                                <th>Ranking</th>
                                <th>Delete</th>
                                <th>Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd gradeX" align="center" ng-repeat="prod in products">
                                <td><%prod.id%></td>
                                <td><%prod.product_name%></td>
                                <td><%prod.description%></td>
                                <td>
                                    <a class="thumbnail">
                                        <img src="{!!asset('<%prod.picture%>')!!}" alt="..." width="80px">
                                    </a>
                                </td>
                                <td><%prod.color%></td>
                                <td><%prod.size%></td>
                                <td><%prod.quantity%></td>
                                <td><%prod.price_unit%></td>
                                <td><%prod.discount%></td>
                                <td><%prod.ranking%></td>
                                <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a style="cursor: pointer;" ng-click="confirmDel(prod.id)">Delete</a></td>
                                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a style="cursor: pointer;" data-toggle="modal" data-target="#myModal" ng-click="edit(prod.id)">Edit</a></td>
                            </tr>
                        </tbody>
                    </table>
                   <posts-pagination class="text-center" ng-hide="abc"></posts-pagination>
                   <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="myModalLabel">Product Edit</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" ng-click="myForm.$setPristine()">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form name="myForm" method='post' enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label class="form-control-label" for="name">Product name</label>
                                        <input type="text" class="form-control" name="product_name" placeholder="Ex: Printed flower" ng-model="prod1.product_name" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Description</label>
                                        <textarea name="description" id="description" cols="76" rows="3" class="form-control" ng-model="prod1.description"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Picture</label>
                                        <input type="file" class="form-control" name="file" accept=".jpg, .jpeg, .png" onchange="angular.element(this).scope().uploadavtar(this.files)" ng-model="prod1.picture" ng-click="myForm.$setDirty()">
                                        <input type="hidden" value="{{ csrf_token() }}" name="_token" >
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Quantity</label>
                                        <input type="text" class="form-control" name="price_unit" ng-model="prod1.quantity">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Price</label>
                                        <input type="text" class="form-control" name="price_unit" ng-model="prod1.price_unit">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Discount</label>
                                        <input type="text" class="form-control" name="discount" ng-model="prod1.discount">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label">Ranking</label>
                                        <input type="text" class="form-control" name="ranking" ng-model="prod1.ranking">
                                    </div>
                                </form>
                              </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal" ng-click="myForm.$setPristine()">Close</button>
                            <button type="button" class="btn btn-primary" ng-disabled="myForm.$pristine || myForm.$invalid" ng-click="save(productID)">Save</button>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
 @endsection