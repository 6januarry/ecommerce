<?php

use Illuminate\Database\Seeder;

class CustomerCareTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customer_care')->insert(
        	[
        		[
        			'id' => 1,
                    'name'=>'Hai',
                    'email'=>'hai@gmail.com',
        			'message' => 'Great',
        			'created_at' => new DateTime(),
        		],
        		[
        			'id' => 2,
                    'name'=>'Nam',
                    'email'=>'nam@gmail.com',
        			'message' => 'Nice',
        			'created_at' => new DateTime(),
        		],
        		[
        			'id' => 3,
                    'name'=>'Thanh',
                    'email'=>'thanh@gmail.com',
        			'message' => 'Not good',
        			'created_at' => new DateTime(),
        		],
        		[
        			'id' => 4,
                    'name'=>'Adam',
                    'email'=>'adam@gmail.com',
        			'message' => 'Bad',
        			'created_at' => new DateTime(),
        		],
        	]);
    }
}
