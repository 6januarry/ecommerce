<?php

use Illuminate\Database\Seeder;

class ShipperTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('shipper')->insert(
        	[
	        	[
	        		'id_user' => 1,
	        		'Phone'=>18001090,
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'id_user' => 2,
	        		'Phone'=>18001091,
	        		'created_at' => new DateTime(),
	        	],
        	]
        );
    }
}
