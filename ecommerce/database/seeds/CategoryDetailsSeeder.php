<?php

use Illuminate\Database\Seeder;

class CategoryDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category_details')->insert(
        	[
	        	[
	        		'id_category' => '1',
	        		'name' => 'Jackets',
	        		'active'=>'1',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'id_category' => '1',
	        		'name' => 'Shirts',
	        		'active'=>'1',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'id_category' => '1',
	        		'name' => 'Jeans',
	        		'active'=>'1',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'id_category' => '1',
	        		'name' => 'Jackets',
	        		'active'=>'1',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'id_category' => '1',
	        		'name' => 'Shorts',
	        		'active'=>'1',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'id_category' => '2',
	        		'name' => 'Sneakers',
	        		'active'=>'1',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'id_category' => '2',
	        		'name' => 'Formal Shoes',
	        		'active'=>'1',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'id_category' => '2',
	        		'name' => 'Casual shoes',
	        		'active'=>'1',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'id_category' => '3',
	        		'name' => 'Bags',
	        		'active'=>'1',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'id_category' => '3',
	        		'name' => 'Belts',
	        		'active'=>'1',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'id_category' => '3',
	        		'name' => 'Socks',
	        		'active'=>'1',
	        		'created_at' => new DateTime(),
	        	],
        	]
        );
    }
}
