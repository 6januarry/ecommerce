<?php

use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert(
        	[
	        	[
	        		'id_customer' => 3,
	        		'id_payment'=>1,
	        		'id_shipper'=>1,
	        		'order_date' => '2017-11-21 06:04:31',
	        		'payment_date'=>'2017-11-21 06:04:31',
	        		'ship_date'=>'2017-11-21 06:04:31',
	        		'address'=>'22 Nguyen Duc Canh',
	        		'paid'=>20000,
	        		'status'=>0,
	        		'created_at'=>'2017-11-21 06:04:31',
	        	],
	        	[
	        		'id_customer' => 3,
	        		'id_payment'=>1,
	        		'id_shipper'=>2,
	        		'order_date' => '2017-11-30 22:10:31',
	        		'payment_date'=>'2017-11-30 22:10:31',
	        		'ship_date'=>'2017-11-11 22:10:31',
	        		'address'=>'22 Nguyen Duc Canh',
	        		'paid'=>20000,
	        		'status'=>0,
	        		'created_at'=>'2017-11-30 22:10:31',
	        	],
	        	[
	        		'id_customer' => 4,
	        		'id_payment'=>2,
	        		'id_shipper'=>1,
	        		'order_date' => '2017-11-01 12:09:31',
	        		'payment_date'=>'2017-11-01 12:09:31',
	        		'ship_date'=>'2017-11-01 12:09:31',
	        		'address'=>'44 May To',
	        		'paid'=>30000,
	        		'status'=>0,
	        		'created_at'=>'2017-11-01 12:09:31',
	        	],
	        	[
	        		'id_customer' => 4,
	        		'id_payment'=>2,
	        		'id_shipper'=>1,
	        		'order_date' => '2017-11-10 02:19:31',
	        		'payment_date'=>'2017-11-10 02:19:31',
	        		'ship_date'=>'2017-11-10 02:19:31',
	        		'address'=>'44 May To',
	        		'paid'=>30000,
	        		'status'=>0,
	        		'created_at'=>'2017-11-10 02:19:31',
	        	],
	        	[
	        		'id_customer' => 3,
	        		'id_payment'=>2,
	        		'id_shipper'=>2,
	        		'order_date' => '2017-11-10 14:30:31',
	        		'payment_date'=>'2017-11-10 14:30:31',
	        		'ship_date'=>'2017-11-10 14:30:31',
	        		'address'=>'22 Nguyen Duc Canh',
	        		'paid'=>20000,
	        		'status'=>0,
	        		'created_at'=>'2017-11-10 14:30:31',
	        	],
	        	[
	        		'id_customer' => 3,
	        		'id_payment'=>2,
	        		'id_shipper'=>2,
	        		'order_date' => '2017-11-10 14:30:31',
	        		'payment_date'=>'2017-11-10 14:30:31',
	        		'ship_date'=>'2017-11-10 14:30:31',
	        		'address'=>'22 Nguyen Duc Canh',
	        		'paid'=>20000,
	        		'status'=>0,
	        		'created_at'=>'2017-11-10 14:30:31',
	        	],
	        	[
	        		'id_customer' => 3,
	        		'id_payment'=>2,
	        		'id_shipper'=>2,
	        		'order_date' => '2017-11-10 14:30:31',
	        		'payment_date'=>'2017-11-10 14:30:31',
	        		'ship_date'=>'2017-11-10 14:30:31',
	        		'address'=>'22 Nguyen Duc Canh',
	        		'paid'=>20000,
	        		'status'=>0,
	        		'created_at'=>'2017-11-10 14:30:31',
	        	],
	        	[
	        		'id_customer' => 3,
	        		'id_payment'=>2,
	        		'id_shipper'=>2,
	        		'order_date' => '2017-11-10 14:30:31',
	        		'payment_date'=>'2017-11-10 14:30:31',
	        		'ship_date'=>'2017-11-10 14:30:31',
	        		'address'=>'22 Nguyen Duc Canh',
	        		'paid'=>20000,
	        		'status'=>0,
	        		'created_at'=>'2017-11-10 14:30:31',
	        	]
        	]
        );
    }
}
