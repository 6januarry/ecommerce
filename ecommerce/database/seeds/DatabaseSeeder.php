<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserTableSeeder::class);
         $this->call(BrandTableSeeder::class);
         $this->call(CategoryTableSeeder::class);
         $this->call(CategoryDetailsSeeder::class);
         $this->call(ShipperTableSeeder::class);
         $this->call(PaymentTableSeeder::class);
         $this->call(ProductsTableSeeder::class);
         $this->call(ProductDetailsTableSeeder::class);
         $this->call(OrdersTableSeeder::class);
         $this->call(OrdersDetailTableSeeder::class);
         $this->call(PictureTableSeeder::class);
         $this->call(SlideTableSeeder::class);
         $this->call(CustomerCareTableSeeder::class);
         $this->call(ImportProductsTableSeeder::class);
         $this->call(ImportProductDetailsTableSeeder::class);
    }
}
