<?php

use Illuminate\Database\Seeder;

class SlideTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          DB::table('slide')->insert(
        	[
        		[
        			'id' => 1,
        			'picture' => 'slide1',
        			'link' => 'ec/template/images/slide/slide1.jpg',
        			'created_at' => new DateTime(),
        		],
        		[
        			'id' => 2,
        			'picture' => 'slide2',
        			'link' => 'ec/template/images/slide/slide2.jpg',
        			'created_at' => new DateTime(),
        		],
        		[
        			'id' => 3,
        			'picture' => 'slide3',
        			'link' => 'ec/template/images/slide/slide3.jpg',
        			'created_at' => new DateTime(),
        		],
        	]
        );
    }
}
