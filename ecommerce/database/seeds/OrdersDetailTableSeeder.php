<?php

use Illuminate\Database\Seeder;

class OrdersDetailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_details')->insert(
        	[
	        	[
	        		'id_order' => 1,
	        		'id_product_details'=>1,
	        		'price'=>10000,
	        		'quantity'=>1,
	        		'total' => 10000,
	        		'discount'=>0,
	        		'created_at'=>new DateTime(),
	        	],
	        	[
	        		'id_order' => 1,
	        		'id_product_details'=>2,
	        		'price'=>10000,
	        		'quantity'=>1,
	        		'total' => 10000,
	        		'discount'=>0,
	        		'created_at'=>new DateTime(),
	        	],
	        	[
	        		'id_order' => 2,
	        		'id_product_details'=>1,
	        		'price'=>10000,
	        		'quantity'=>2,
	        		'total' => 20000,
	        		'discount'=>0,
	        		'created_at'=>new DateTime(),
	        	],
	        	[
	        		'id_order' => 3,
	        		'id_product_details'=>3,
	        		'price'=>30000,
	        		'quantity'=>1,
	        		'total' => 30000,
	        		'discount'=>0,
	        		'created_at'=>new DateTime(),
	        	],
	        	[
	        		'id_order' => 4,
	        		'id_product_details'=>3,
	        		'price'=>30000,
	        		'quantity'=>1,
	        		'total' => 30000,
	        		'discount'=>0,
	        		'created_at'=>new DateTime(),
	        	],
	        	[
	        		'id_order' => 5,
	        		'id_product_details'=>1,
	        		'price'=>10000,
	        		'quantity'=>2,
	        		'total' => 20000,
	        		'discount'=>0,
	        		'created_at'=>new DateTime(),
	        	],
        	]
        );
    }
}
