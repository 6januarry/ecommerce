<?php

use Illuminate\Database\Seeder;

class PaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment')->insert(
        	[
	        	[
	        		'payment_type' => 'COD',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'payment_type' => 'Banking',
	        		'created_at' => new DateTime(),
	        	]
        	]
        );
    }
}
