<?php

use Illuminate\Database\Seeder;

class ProductDetailsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('product_details')->insert(
        	[
	        	[
	        		'id_products' => 1,
	        		'color'=>'Red',
	        		'size'=>'S',
	        		'quantity'=>'5',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'id_product' => 1,
	        		'color'=>'Red',
	        		'size'=>'XL',
	        		'quantity'=>'5',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'id_product' => 2,
	        		'color'=>'Blue',
	        		'size'=>'M',
	        		'quantity'=>'5',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'id_product' => 2,
	        		'color'=>'Blue',
	        		'size'=>'L',
	        		'quantity'=>'5',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'id_product' => 3,
	        		'color'=>'White',
	        		'size'=>'41',
	        		'quantity'=>'10',
	        		'created_at' => new DateTime(),
	        	]
        	]
        );
    }
}
