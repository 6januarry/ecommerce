<?php

use Illuminate\Database\Seeder;

class PictureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('picture')->insert(
        	[
        		[
        			'id' => 1,
        			'id_products' => 1,
        			'link' => 'ec/template/images/products-images/product1.jpg',
        			'created_at' => new DateTime(),
        		],
        		[
        			'id' => 2,
        			'id_products' => 2,
        			'link' => 'ec/template/images/products-images/product2.jpg',
        			'created_at' => new DateTime(),
        		],
        		[
        			'id' => 3,
        			'id_products' => 3,
        			'link' => 'ec/template/images/products-images/product3.jpg',
        			'created_at' => new DateTime(),
        		],
        	]);
    }
}
