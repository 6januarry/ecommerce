<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('category')->insert(
        	[
	        	[
	        		'category_name' => 'Clothing',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'category_name' => 'Shoes',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'category_name' => 'Accessories',
	        		'created_at' => new DateTime(),
	        	]
        	]
        );
    }
}
