<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert(
        	[
	        	[
	        		'product_name' => 'Printed flower',
	        		'id_category' => 1,
	        		'description' => 'Comfortable, easy to wear',
	        		'price_unit'=>10000,
	        		'discount'=>0,
	        		'ranking'=>10,
	        		'picture'=>'uploads/picture1.png',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'product_name' => 'Printed heroes',
	        		'id_category' => 2,
	        		'description' => 'Comfortable, easy to wear',
	        		'price_unit'=>10000,
	        		'discount'=>0,
	        		'ranking'=>9,
	        		'picture'=>'uploads/picture1.png',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'product_name' => 'Sneaker',
	        		'id_category' => 3,
	        		'description' => 'Comfortable, suit for many occasions',
	        		'price_unit'=>30000,
	        		'discount'=>0,
	        		'ranking'=>5,
	        		'picture'=>'uploads/picture1.png',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'product_name' => 'Printed flower',
	        		'id_category' => 1,
	        		'description' => 'Comfortable, easy to wear',
	        		'price_unit'=>10000,
	        		'discount'=>0,
	        		'ranking'=>10,
	        		'picture'=>'uploads/picture1.png',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'product_name' => 'Printed flower',
	        		'id_category' => 1,
	        		'description' => 'Comfortable, easy to wear',
	        		'price_unit'=>10000,
	        		'discount'=>0,
	        		'ranking'=>10,
	        		'picture'=>'uploads/picture1.png',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'product_name' => 'Printed flower',
	        		'id_category' => 1,
	        		'description' => 'Comfortable, easy to wear',
	        		'price_unit'=>10000,
	        		'discount'=>0,
	        		'ranking'=>10,
	        		'picture'=>'uploads/picture1.png',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'product_name' => 'Printed flower',
	        		'id_category' => 1,
	        		'description' => 'Comfortable, easy to wear',
	        		'price_unit'=>10000,
	        		'discount'=>0,
	        		'ranking'=>10,
	        		'picture'=>'uploads/picture1.png',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'product_name' => 'Printed flower',
	        		'id_category' => 1,
	        		'description' => 'Comfortable, easy to wear',
	        		'price_unit'=>10000,
	        		'discount'=>0,
	        		'ranking'=>10,
	        		'picture'=>'uploads/picture1.png',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'product_name' => 'Printed flower',
	        		'id_category' => 1,
	        		'description' => 'Comfortable, easy to wear',
	        		'price_unit'=>10000,
	        		'discount'=>0,
	        		'ranking'=>10,
	        		'picture'=>'uploads/picture1.png',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'product_name' => 'Printed flower',
	        		'id_category' => 1,
	        		'description' => 'Comfortable, easy to wear',
	        		'price_unit'=>10000,
	        		'discount'=>0,
	        		'ranking'=>10,
	        		'picture'=>'uploads/picture1.png',
	        		'created_at' => new DateTime(),
	        	],
	        	[
	        		'product_name' => 'Printed flower',
	        		'id_category' => 1,
	        		'description' => 'Comfortable, easy to wear',
	        		'price_unit'=>10000,
	        		'discount'=>0,
	        		'ranking'=>10,
	        		'picture'=>'uploads/picture1.png',
	        		'created_at' => new DateTime(),
	        	]
        	]
        );
    }
}
