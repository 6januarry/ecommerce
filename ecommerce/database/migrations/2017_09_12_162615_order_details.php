<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('id');
             $table->integer('id_order')->unsigned();
            $table->foreign('id_order')->references('id')->on('orders');
             $table->integer('id_product_details')->unsigned();
            $table->foreign('id_product_details')->references('id')->on('product_details');
            $table->integer('price');
            $table->integer('total');
            $table->integer('quantity');
            $table->integer('discount');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
