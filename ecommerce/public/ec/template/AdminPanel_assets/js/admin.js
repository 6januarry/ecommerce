var app = angular.module('myAdmin', ['doowb.angular-pusher'],function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');

}).constant('API', 'http://localhost/ecommerce_test2/public/admin_panel/').config(['PusherServiceProvider',
  function(PusherServiceProvider) {
    PusherServiceProvider
    .setToken('2c17ffb3164a19487fcd')
    .setOptions({
    	'cluster' :'ap1',
    	'encrypted' : true,
    });
  }
]);

app.controller('controller',['$scope','$http','API','Pusher',function($scope,$http,API,Pusher){
    var hai=function(){
        $http.get(API + 'order_today').then(function success(response){
            $scope.orders=response.data;
            //console.log(response.data);
        },function error(response){
            //console.log(response);
        });
        $http.get(API + 'order_monthly').then(function success(response){
            $scope.orders=response.data;
            //console.log(response.data);
        },function error(response){
            //console.log(response);
        });
        $http.get(API + 'revenue').then(function success(response){
            $scope.orders=response.data;
            //console.log(response.data);
        },function error(response){
            //console.log(response);
        });

    };  
    hai();
    Pusher.subscribe('admin', 'orderstoday', function (item) {
    // an item was updated. find it in our list and update it.
        //console.log(item);
        $scope.orderstoday = item;
    }
  );
    Pusher.subscribe('admin', 'ordersmonthly', function (item) {
    // an item was updated. find it in our list and update it.
        //console.log(item);
        $scope.ordersmonthly = item;
    }
  );
    Pusher.subscribe('admin', 'revenue', function (item) {
    // an item was updated. find it in our list and update it.
        //console.log(item);
        $scope.revenue = item;
    }
  );
    hai();
}]);

app.controller('controller1',['$scope','$http','API','$window',function($scope,$http,API,$window){
    var hai=function(){
        $http.get(API + 'category').then(function success(response){
            $scope.categories = response.data;
            //console.log(response.data);
        },function error(response){
            //console.log(response);
        });
    };  
    hai();
    $scope.edit=function(id)
    {
        $http.get(API + 'category_details/' + id).then(function(response){
            //console.log(response.data[0].active);
            $scope.cate_details=response.data[0];
            $scope.cate_details.active=$scope.cate_details.active.toString();
            //var a=$scope.cate_details
            console.log($scope.cate_details.active);
        },function error(response){

        });
        $scope.id=id;
    }
    $scope.save=function(id)
    {
        var url=API + 'category_details/'+ id;
            var data1=$.param($scope.cate_details);
            console.log(data1);
            $http({
                method : 'POST',
                url : url,
                //data : $httpParamSerializer(data1),
                data: data1,
                headers : {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function success(response){
                location.reload();
            });
    }
    $scope.confirmDel=function(id)
    {
        var isConfirm = confirm('Do you want to delete this category');
        if(isConfirm)
        {
            $http.get(API + 'category_del/' + id).then(function success(response){
                //location.reload();
                $window.location=API+'cate_list';
            });
        }
    }
}]);

app.controller('controller2',['$scope','$http','API',function($scope,$http,API){
    $scope.add={};
    $scope.add.id_category='1';
    $scope.add.active='1';
    $scope.addCate=function()
    {
        console.log($scope.add);
        var url=API + "category_add";
            var data1=$.param($scope.add);
            console.log(data1);
            $http({
                method : 'POST',
                url : url,
                //data : $httpParamSerializer(data1),
                data: data1,
                headers : {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function success(response){
                location.reload();
            });
    }
}]);

app.controller('controller3',['$scope','$http','API',function($scope,$http,API){
    $scope.product={};
    $scope.product.cate='1';
    $http.get(API + 'cate_details/' + '1').then(function success(response){
                //console.log(response.data);
                $scope.product.cate_details = response.data;
                //console.log($scope.product.cate_details);
            });
    $scope.change=function()
    {
        if($scope.product.cate=='1')
        {
            $http.get(API + 'cate_details/' + '1').then(function success(response){
                //console.log(response.data);
                $scope.product.cate_details = response.data;
                console.log($scope.product.cate_details);
            });
        }
        if($scope.product.cate=='2')
        {
            $http.get(API + 'cate_details/' + '2').then(function success(response){
                //console.log(response.data);
                $scope.product.cate_details = response.data;
                console.log($scope.product.cate_details);
            });
        }
        if($scope.product.cate=='3')
        {
            $http.get(API + 'cate_details/' + '3').then(function success(response){
                //console.log(response.data);
                $scope.product.cate_details = response.data;
                console.log($scope.product.cate_details);
            });
        }
    }
    $scope.abc=true;
    var pageNumber=1;
    $scope.totalPages = 0;
    $scope.currentPage = 1;
    $scope.range = [];
    $scope.getProds = function (pageNumber1) {
      if (pageNumber1 === undefined) {
          pageNumber1 == '1';
      }
      pageNumber=pageNumber1;
      hai();
    }
    var hai=function(){
            $http.get(API + 'products/' +$scope.id+ '?page=' + pageNumber).then(function success(response){
                $scope.products=response.data.data;
                $scope.totalPages   = response.data.last_page;
                $scope.currentPage  = response.data.current_page;
                console.log($scope.id);
            var pages = [];
              for (var i = 1; i <= response.data.last_page; i++) {
                pages.push(i);
              }
              $scope.range = pages;
                //console.log(response.data);
            },function error(response){
                //console.log(response);
            });
            
        };  
    $scope.showProds=function()
    {
        $scope.abc=false;
        
        hai();
    }
    $scope.edit=function(id)
    {
        $http.get(API + 'product_get/' + id).then(function(response){
            //console.log(response.data[0].active);
            $scope.prod1=response.data[0];
            //$scope.cate_details.active=$scope.cate_details.active.toString();
            //var a=$scope.cate_details
            console.log($scope.prod1);
        },function error(response){

        });
        $scope.productID=id;
    }
    $scope.confirmDel=function(id)
    {
        $http.get(API + 'product_del/' + id).then(function success(response){
                $scope.checkorder=response.data;
            });
        if($scope.checkorder==0)
        {
           var isConfirm = confirm('Do you want to delete this product');
            if(isConfirm)
            {
                $http.get(API + 'product_del/' + id).then(function success(response){
                    location.reload();
                });
            } 
        }
        else
            alert('There are orders with that product, so cannot delete');
    }
    $scope.save=function(id)
    {
        var a=Date.now()+'.jpg';
        if(file!=null)
        {
            var fd = new FormData();
            fd.append("file", file[0],a);
            $http.post(API+'image_add', fd, {
                withCredentials: true,
                headers: {'Content-Type': undefined },
                transformRequest: angular.identity
            }).then(function successCallback(response) {
                alert(response);
            }, function errorCallback(response) {
                alert(response);
            });
            $scope.prod1.picture='uploads/'+a;
        }
        var url=API + 'product_post/'+ id;
            var data1=$.param($scope.prod1);
            console.log(data1);
            $http({
                method : 'POST',
                url : url,
                //data : $httpParamSerializer(data1),
                data: data1,
                headers : {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function success(response){
                location.reload();
            });
            console.log(file);
    }
    var file=null;
    $scope.uploadavtar = function(files) {
            file=files;
    }
}]);
app.directive('postsPagination', function(){
   return {
      restrict: 'E',
      template: '<ul class="pagination">'+
        '<li ng-show="currentPage != 1" class="page-item"><a class="page-link" href="javascript:void(0)" ng-click="getProds(1)">«</a></li>'+
        '<li class="page-item" ng-show="currentPage != 1"><a class="page-link" href="javascript:void(0)" ng-click="getProds(currentPage-1)">‹ Prev</a></li>'+
        '<span>  </span>'+
        '<li class="page-item" ng-repeat="i in range" ng-class="{active : currentPage == i}">'+
            '<a class="page-link" href="javascript:void(0)" ng-click="getProds(i)"><% i %></a>'+
        '</li>'+
        '<li class="page-item" ng-show="currentPage != totalPages"><a class="page-link" href="javascript:void(0)" ng-click="getProds(currentPage+1)">Next ›</a></li>'+
        '<li class="page-item" ng-show="currentPage != totalPages"><a class="page-link" href="javascript:void(0)" ng-click="getProds(totalPages)">»</a></li>'+
      '</ul>'
   };
});


app.controller('controller4',['$scope','$http','API',function($scope,$http,API){
    var pageNumber=1;
    $scope.totalPages = 0;
    $scope.currentPage = 1;
    $scope.range = [];
    $scope.getUsers = function (pageNumber1) {
      if (pageNumber1 === undefined) {
          pageNumber1 == '1';
      }
      pageNumber=pageNumber1;
      a();
  }
    var a=function(){
        $http.get(API + 'list_users'+ '?page=' + pageNumber).then(function success(response){
                $scope.users=response.data.data;
                console.log(response.data);
                $scope.totalPages   = response.data.last_page;
                $scope.currentPage  = response.data.current_page;
            var pages = [];
              for (var i = 1; i <= response.data.last_page; i++) {
                pages.push(i);
              }
              $scope.range = pages;
                //console.log(response.data);
            },function error(response){
                //console.log(response);
            });
        };
    a();
    $scope.confirmDel=function(id)
    {
        var isConfirm = confirm('Do you want to delete this user');
            if(isConfirm)
            {
                $http.get(API + 'user_del/' + id).then(function success(response){
                    //$scope.check=response.data;
                    if(response.data=='true')
                        location.reload();
                    else
                        alert('Can not delete this user');
                });
            }     
    }
    $scope.edit=function(id)
    {
        $http.get(API + 'user_details/' + id).then(function(response){
            //console.log(response.data[0].active);
            $scope.user_details=response.data;
            $scope.user_details.active=$scope.user_details.active.toString();
            $scope.user_details.level=$scope.user_details.level.toString();
            //var a=$scope.cate_details
            console.log($scope.user_details);
            //$scope.user_details.name=$scope.user_details.first_name+' '+$scope.user_details.last_name;
        },function error(response){

        });
        $scope.id=id;
    }
    $scope.save=function(id)
    {
        var url=API + 'user_details/'+ id;
            var data1=$.param($scope.user_details);
            console.log(data1);
            $http({
                method : 'POST',
                url : url,
                //data : $httpParamSerializer(data1),
                data: data1,
                headers : {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function success(response){
                location.reload();
            });
    }
}]);
app.directive('paginationUser', function(){
   return {
      restrict: 'E',
      template: '<ul class="pagination">'+
        '<li ng-show="currentPage != 1" class="page-item"><a class="page-link" href="javascript:void(0)" ng-click="getUsers(1)">«</a></li>'+
        '<li class="page-item" ng-show="currentPage != 1"><a class="page-link" href="javascript:void(0)" ng-click="getUsers(currentPage-1)">‹ Prev</a></li>'+
        '<span>  </span>'+
        '<li class="page-item" ng-repeat="i in range" ng-class="{active : currentPage == i}">'+
            '<a class="page-link" href="javascript:void(0)" ng-click="getUsers(i)"><% i %></a>'+
        '</li>'+
        '<li class="page-item" ng-show="currentPage != totalPages"><a class="page-link" href="javascript:void(0)" ng-click="getUsers(currentPage+1)">Next ›</a></li>'+
        '<li class="page-item" ng-show="currentPage != totalPages"><a class="page-link" href="javascript:void(0)" ng-click="getUsers(totalPages)">»</a></li>'+
      '</ul>'
   };
});

app.controller('controller5',['$scope','$http','API',function($scope,$http,API){
    $scope.add={};
    $scope.add.level='0';
    $scope.addUser=function()
    {
        if($scope.add.password == $scope.check_password)
        {
                $http.get(API + 'check_mail/' + $scope.add.email).then(function success(response){
                if(response.data=='true')
                {
                   var url=API + "user_add";
                   var data1=$.param($scope.add);
                    console.log(data1);
                    $http({
                        method : 'POST',
                        url : url,
                        //data : $httpParamSerializer(data1),
                        data: data1,
                        headers : {'Content-Type': 'application/x-www-form-urlencoded'}
                    }).then(function success(response){
                        location.reload();
                    });
                }
                else
                {
                    alert('This email already exists');
                }    
            })
        }
        else
            alert('Passwords do not coincide');
        //console.log($scope.add);

    }
}]);












