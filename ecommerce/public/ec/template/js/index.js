var app = angular.module('my-app', [],function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');
}).constant('API', 'http://localhost/ecommerce/public/');


app.controller('CoursesController', [ '$http', '$scope','API','$window', function($http, $scope, API,$window){
  $scope.BestSellers = [];
  $scope.totalPages = 0;
  $scope.currentPage = 1;
  $scope.range = [];
  pageNumber=1;
  // data list course of page
  $scope.getCourse = function (pageNumber1) {
      if (pageNumber1 === undefined) {
          pageNumber1 == '1';
      }
      pageNumber=pageNumber1;
      bestseller();
      product();
    }
      var bestseller = function()
      {
      	
      $http.get( API + 'getBestSellers' + '?page=' + pageNumber).then(function(response) {
          $scope.BestSellers  = response.data.data;
          $scope.totalPages   = response.data.last_page;
          $scope.currentPage  = response.data.current_page;
          console.log($scope.BestSellers);
          console.log($scope.totalPages);
          console.log($scope.currentPage);
          // Pagination Range
          var pages = [];

          for (var i = 1; i <= response.data.last_page; i++) {
            pages.push(i);
          }
          $scope.range = pages;
      },function error(response){
      	console.log(response);
      });
  };
  var product = function()
      {
        
      $http.get( API + 'getProducts' + '?page=' + pageNumber).then(function(response) {
          $scope.Products  = response.data.data;
          $scope.totalPages   = response.data.last_page;
          $scope.currentPage  = response.data.current_page;
          console.log($scope.Products);
          console.log($scope.totalPages);
          console.log($scope.currentPage);
          // Pagination Range
          var pages = [];

          for (var i = 1; i <= response.data.last_page; i++) {
            pages.push(i);
          }
          $scope.range = pages;
      },function error(response){
        console.log(response);
      });
     
  };

   $scope.product_details=function(id)
    {
        // $http.get(API + 'product_detail/' + id).then(function(response){
        //     $scope.product_details=response.data.data;
            
            $window.location=API + 'product_detail/' + id;
        // },function error(response){

        // });
        // $scope.id=id;
        
    }

  product();
  bestseller();
  
    
}]);

// app.controller('DetailsController', ['$http', '$scope','API', function($http, $scope, API,$timeout){
//      $scope.edit=function(id)
//     {
//         $http.get(API + 'product_detail/' + id).then(function(response){
//             //console.log(response.data[0].active);
//             $scope.cate_details=response.data[0];
//             $scope.cate_details.active=$scope.cate_details.active.toString();
//             //var a=$scope.cate_details
//             console.log($scope.cate_details.active);
//         },function error(response){

//         });
//         $scope.id=id;
//     }
  
// }])

app.directive("owlCarousel", function() {
  return {
    restrict: 'E',
    transclude: false,
    link: function (scope) {
      scope.initCarousel = function(element) {
        // provide any default options you want
        var defaultOptions = {
          items: 4
      , itemsDesktop: [1024, 4]
      , itemsDesktopSmall: [900, 3]
      , itemsTablet: [600, 2]
      , itemsMobile: [320, 1]
      , navigation: !0
      , navigationText: ['<a class="flex-prev"></a>', '<a class="flex-next"></a>']
      , slideSpeed: 500
      , pagination: !1
        };
        var customOptions = scope.$eval($(element).attr('data-options'));
        // combine the two options objects
        for(var key in customOptions) {
          defaultOptions[key] = customOptions[key];
        }
        // init carousel
        $(element).owlCarousel(defaultOptions);
      };
    }
  };
})
app.directive('owlCarouselItem', [function() {
  return {
    restrict: 'A',
    transclude: false,
    link: function(scope, element) {
      // wait for the last item in the ng-repeat then call init
      if(scope.$last) {
        scope.initCarousel(element.parent());
      }
    }
  };
}]);

// app.controller('CoursesController', [ '$http', '$scope', function($http, $scope, API,$timeout){
//   $scope.Products = [];
//   $scope.totalPages = 0;
//   $scope.currentPage = 1;
//   $scope.range = [];
//   pageNumber=1;
//   // data list course of page
//   $scope.getCourse = function (pageNumber1) {
//       if (pageNumber1 === undefined) {
//           pageNumber1 == '1';
//       }
//       pageNumber=pageNumber1;
//       product();
//     }
//       var product = function()
//       {
        
//       $http.get( 'http://localhost/ecommerce/public/getProducts' + '?page=' + pageNumber).then(function(response) {
//           $scope.Products  = response.data.data;
//           $scope.totalPages   = response.data.last_page;
//           $scope.currentPage  = response.data.current_page;
//           console.log($scope.Products);
//           console.log($scope.totalPages);
//           console.log($scope.currentPage);
//           // Pagination Range
//           var pages = [];

//           for (var i = 1; i <= response.data.last_page; i++) {
//             pages.push(i);
//           }
//           $scope.range = pages;
//       },function error(response){
//         console.log(response);
//       });
     
//   };
//   product();
// }]);


app.controller('MessageController',['$http','$scope',function($http,$scope,API){
  $scope.message={};
    console.log($scope.message);
}]);