var app = angular.module('myDeliver', ['doowb.angular-pusher'],function($interpolateProvider) {
    $interpolateProvider.startSymbol('<%');
    $interpolateProvider.endSymbol('%>');

}).constant('API', 'http://localhost/ecommerce/public/deliver_admin/').config(['PusherServiceProvider',
  function(PusherServiceProvider) {
    PusherServiceProvider
    .setToken('2c17ffb3164a19487fcd')
    .setOptions({
    	'cluster' :'ap1',
    	'encrypted' : true,
    });
  }
]);

app.controller('controller',['$scope','$http','API','Pusher',function($scope,$http,API,Pusher){
	pageNumber=1;
	$scope.totalPages = 0;
  	$scope.currentPage = 1;
  	$scope.range = [];
  	pageNumber=1;
  	$scope.getOrder = function (pageNumber1) {
      if (pageNumber1 === undefined) {
          pageNumber1 == '1';
      }
      pageNumber=pageNumber1;
      hai();
    }
	var hai=function(){
		$http.get(API + 'getorders' + '?page=' + pageNumber).then(function success(response){
			$scope.orders=response.data.data;
			$scope.totalPages   = response.data.last_page;
        	$scope.currentPage  = response.data.current_page;
        	//console.log(response);
        var pages = [];
          for (var i = 1; i <= response.data.last_page; i++) {
            pages.push(i);
          }
          $scope.range = pages;
			//console.log(response.data);
		},function error(response){
			//console.log(response);
		});
		
	};	
	hai();
	Pusher.subscribe('deliver', 'neworders', function (item) {
    // an item was updated. find it in our list and update it.
    	console.log(item);
        $scope.orders = item.data;
        $scope.totalPages   = item.last_page;
        $scope.currentPage  = item.current_page;
        var pages = [];
          for (var i = 1; i <= item.last_page; i++) {
            pages.push(i);
          }
          $scope.range = pages;
          console.log($scope.totalPages);
          console.log($scope.currentPage);
    }
  );
	hai();
	$scope.state=function(state,id)
	{
		$scope.a;
		if(state=='receive')
		{
			//$http.post(API + 'postStatus')
			$scope.a=1;
			
		}
		else $scope.a=2;
		var data1=$scope.a;
		$http({
				method : 'POST',
				url : API + 'postStatus/'+id,
				//data : $httpParamSerializer(data1),
				data: data1,
			}).then(function success(response){
				location.reload();
				console.log(response);
			});
			console.log(id);
	}
}]);

app.directive('postsPagination', function(){
   return {
      restrict: 'E',
      template: '<ul class="pagination">'+
        '<li ng-show="currentPage != 1" class="page-item"><a class="page-link" href="javascript:void(0)" ng-click="getOrder(1)">«</a></li>'+
        '<li class="page-item" ng-show="currentPage != 1"><a class="page-link" href="javascript:void(0)" ng-click="getOrder(currentPage-1)">‹ Prev</a></li>'+
        '<span>  </span>'+
        '<li class="page-item" ng-repeat="i in range" ng-class="{active : currentPage == i}">'+
            '<a class="page-link" href="javascript:void(0)" ng-click="getOrder(i)"><% i %></a>'+
        '</li>'+
        '<li class="page-item" ng-show="currentPage != totalPages"><a class="page-link" href="javascript:void(0)" ng-click="getOrder(currentPage+1)">Next ›</a></li>'+
        '<li class="page-item" ng-show="currentPage != totalPages"><a class="page-link" href="javascript:void(0)" ng-click="getOrder(totalPages)">»</a></li>'+
      '</ul>'
   };
});

app.controller('controller1',['$scope','$http','API','Pusher',function($scope,$http,API,Pusher){
	var hai_index=function(){
		$http.get(API + 'countorders').then(function success(response){
			//$scope.count=response.data;
			//console.log(response.data);
		},function error(response){
			//console.log(response);
		});
		$http.get(API + 'countcancel').then(function success(response){
			//$scope.cancel=response.data;
			//console.log(response.data);
		},function error(response){
			//console.log(response);
		});
		$http.get(API + 'countproducts').then(function success(response){
			//$scope.countproducts=response.data;
			//console.log(response.data);
		},function error(response){
			//console.log(response);
		});
		//$timeout(hai_index, 5000);
	};
	hai_index();
	Pusher.subscribe('deliver', 'count', function (item) {
    // an item was updated. find it in our list and update it.
    	//console.log(item);
        $scope.count = item;
    }
  	);
  	Pusher.subscribe('deliver', 'cancel', function (item) {
    // an item was updated. find it in our list and update it.
    	//console.log(item);
        $scope.cancel = item;
    }
  );
	Pusher.subscribe('deliver', 'products', function (item) {
    // an item was updated. find it in our list and update it.
    	//console.log(item);
        $scope.countproducts = item;
    }
  );
	hai_index();
}]);

app.controller('controller2',['$scope','$http','API','$timeout',function($scope,$http,API,$timeout){
	// var a=function(){
	// 	while(angular.isNumber($scope.myID)==false)
	// 	{
	// 		$timeout(a, 5000);
	// 		console.log($scope.myID);
	// 	}
	// 	$http.get(API + 'getcompany/'+ $scope.myID).then(function success(response){
	// 		$scope.company=response.data;
	// 		console.log(response.data);
	// 	},function error(response){
	// 		console.log(response);
	// 	});
	// };
	// a();
	$scope.reset=function()
	{
		$scope.pass=[];
	}
	$scope.send=function()
	{
		var url= API + 'changepass';
			var data1=$.param($scope.pass);
			console.log(data1);
			$http({
				method : 'POST',
				url : url,
				//data : $httpParamSerializer(data1),
				data: data1,
				headers : {'Content-Type': 'application/x-www-form-urlencoded'}
			}).then(function success(response){
				location.reload();
				console.log(response);
			});
	}
	$scope.check=function()
	{
		if($scope.new==$scope.re)
			return false;
		return true;
	}
	$scope.a=function()
	{
		// $http.get(API + 'getcompany/'+ $a).then(function success(response){
		// 	$scope.company=response.data[0].company_name;
		// 	console.log(response.data);
		// },function error(response){
		// 	console.log(response);
		// });
		// $http({
		//   method: 'GET',
		//   url: "http://ecommerce2.dev/deliver_admin/getcompany/1",
		//   headers: {
  //           //'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
  //            'X-Requested-With': 'XMLHttpRequest',
  //       }
		// }).then(function successCallback(response) {
		//     $scope.company=response.data[0].company_name;
		//   }, function errorCallback(response) {
		//     console.log(response);
		//   });
	}
}]);
	