<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('deliver','Deliver@getLogin')->name('getLogin');
Route::post('deliver','Deliver@postLogin')->name('postLogin');

Route::get('deliver/logout','Deliver@getLogOut')->name('getLogOut');

// Route::get('test',function(){
//    $orders = DB::table('orders')
//             ->join('ec_users', 'orders.id_customer', '=', 'ec_users.id')
//             ->select('orders.*', 'ec_users.first_name', 'ec_users.last_name')
//             ->where('orders.status','=','0')
//             ->orderBy('id')
//             ->paginate(5);
//             return $orders;
// });
    Route::post('test',function(){
        $a=DB::table('orders')->where('id','=',1)->update(['status'=>1]);
    });
Route::middleware('auth')->group(function () {
    Route::prefix('deliver_admin')->group(function(){
    	Route::get('/',function(){
    		return view('admindeliver.index');
    	})->name('index');
    	Route::get('accounts',function(){
    		return view('admindeliver.accounts');
    	})->name('accounts');
    	Route::get('manage',function(){
    		return view('admindeliver.manage');
    	})->name('manage');
    	Route::get('statistic','Deliver@getChart')->name('statistic');
        Route::get('getorders','Deliver@getList');
        Route::get('countorders','Deliver@getCount');
        Route::get('countcancel','Deliver@getCancel');
        Route::get('countproducts','Deliver@getCountProducts');
        Route::get('getcompany/{id}','Deliver@getCompany');
        Route::post('postStatus/{id}','Deliver@postStatus');
        Route::post('changepass','Deliver@changePass');
    });
});

// Route cho trang chinh ban hang
// Route::middleware('page')->group(function () {
Route::prefix('')->group(function(){
    route::get('/',function(){
            return view('page.content.trangchu');
        })->name('index_page');
    route::get('about_us',function(){
            return view('page.content.about_us');
        })->name('about_us');
    route::get('checkout',function(){
            return view('page.content.checkout');
        })->name('checkout');
     route::get('wishlist',function(){
            return view('page.content.wishlist');
        })->name('wishlist');
      route::get('grid',function(){
            return view('page.content.grid');
        })->name('grid');
       route::get('quick_view',function(){
            return view('page.content.quick_view');
        })->name('quick_view');
      route::get('list',function(){
            return view('page.content.list');
        })->name('list');
      Route::get('getBestSellers', 'CourseController@getBestSellers');
      Route::get('getSlide', 'CourseController@getSlide');
      Route::get('getProducts', 'CourseController@getProducts');
      Route::get('getCategory', 'CourseController@getCategory');
      Route::get('product_detail/{id}','CourseController@getProductDetails');
});
// });


Route::get('admin','Admin@getLogin')->name('getAdminLogin');
Route::post('admin','Admin@postLogin')->name('postAdminLogin');

Route::get('admin/logout','Admin@getLogOut')->name('getAdminLogOut');
Route::middleware('checkauth')->group(function(){
    Route::prefix('admin_panel')->group(function(){
        // Route::get('/',function(){
        //         return view('admin.login');
        //     })->name('login_admin');
        // Route::get('/index',function(){
        //         return view('admin.layout.index');
        //     })->name('index_admin');
        Route::get('/','Admin@getChart')->name('index_admin');
         Route::get('cate_list',function(){
                return view('admin.category.cate_list');
            })->name('cate_list');
        Route::get('cate_edit',function(){
                return view('admin.category.cate_edit');
            })->name('cate_edit');
        Route::get('cate_add',function(){
                return view('admin.category.cate_add');
            })->name('cate_add');
        Route::get('pro_list',function(){
                return view('admin.products.pro_list');
            })->name('pro_list');
        Route::get('pro_edit',function(){
                return view('admin.products.pro_edit');
            })->name('pro_edit');
        Route::get('pro_add',function(){
                return view('admin.products.pro_add');
            })->name('pro_add');
        Route::get('user_list',function(){
                return view('admin.users.user_list');
            })->name('user_list');
        Route::get('user_edit',function(){
                return view('admin.users.user_edit');
            })->name('user_edit');
        Route::get('user_add',function(){
                return view('admin.users.user_add');
            })->name('user_add');
        Route::get('order_today','Admin@getOrderToday');
        Route::get('order_monthly','Admin@getOrderMonthly');
        Route::get('revenue','Admin@revenue');
        Route::get('category','Admin@getCategory');
        Route::get('category_details/{id}','Admin@cateEdit');
        Route::post('category_details/{id}','Admin@updateEdit');
        Route::post('category_add','Admin@addCate');
        Route::get('category_del/{id}','Admin@delCate');
        Route::get('cate_details/{id}','Admin@getCateDetails');
        Route::get('products/{id}','Admin@getProds');
        Route::post('image_add','Admin@addImage');
        Route::post('product_edit','Admin@postEditProduct');
        Route::get('product_get/{id}','Admin@getProduct');
        Route::post('product_post/{id}','Admin@postProduct');
        Route::get('product_del/{id}','Admin@delProds');
        Route::get('find_prods_orders/{id}','Admin@findProdsOrders');
        Route::get('list_users','Admin@getUsers');
        //Route::get('check_user','Admin@checkUser');
        Route::get('user_del/{id}','Admin@delUser');
        Route::get('user_details/{id}','Admin@getUserDetails');
        Route::post('user_details/{id}','Admin@postUserDetails');
        Route::post('user_add','Admin@postUserAdd');
        Route::get('check_mail/{mail}','Admin@getMail');
    });
});
//Route::post('deliver_admin/changepass','Deliver@changePass');
